#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct pcb
{
 char id[10];      //进程名
 char equip[10];    //申请的设备名
 struct pcb *Next;
}pcb,*PCB;

typedef struct
{
 char chanid[10];    //通道标识符
 int state;      //通道状态
 PCB useing;      //正在使用该通道的进程
 PCB chctr,chctf;    //阻塞队首
}chct,*CHCT;
chct chct_block;
chct chct_run;
typedef struct
{
 char controid[10];   //控制器标示
 int state;      //控制器状态
 CHCT direct;     //通道表指针
 PCB useing;      //正在使用该控制器的进程
 PCB coctr,coctf;    //阻塞队首
}coct,*COCT;
coct coct_block;


typedef struct
{
 char type;      //设备类型
 char equipid[10];    //设备名
 int state;      //设备状态
 COCT direct;     //控制器指针
 PCB useing;      //正在使用该设备的进程
 PCB dctr,dctf;    //阻塞队首
}Dct,*DCT;
Dct Dct_block;

typedef struct sdt
{
 char type;      //设备类型
 char equipid[10];    //设备名
 DCT dct;  //设备的DCT
 struct sdt *next;
}sdt,*SDT;


SDT SDT_table;
char tempco[10],tempch[10],Name[10];
//键盘k鼠标m打印机p屏幕t
DCT k=(DCT)malloc(sizeof(Dct));
DCT m=(DCT)malloc(sizeof(Dct));
DCT p=(DCT)malloc(sizeof(Dct));
DCT t=(DCT)malloc(sizeof(Dct));

//控制器
COCT c1=(COCT)malloc(sizeof(coct));
COCT c2=(COCT)malloc(sizeof(coct));
COCT c3=(COCT)malloc(sizeof(coct));

//通道
CHCT h1=(CHCT)malloc(sizeof(chct));
CHCT h2=(CHCT)malloc(sizeof(chct));

void init_creat_SDT(SDT *head)
{
    SDT s,r;
    int i;
    *head=(SDT)malloc(sizeof(sdt));
     r=*head;

       s=(SDT)malloc(sizeof(sdt));
       strcpy(s->equipid,"K");
       s->type='1';
       s->dct=k;

       r->next=s;
       r=s;

       s=(SDT)malloc(sizeof(sdt));
       strcpy(s->equipid,"M");
       s->type='2';
       s->dct=m;

       r->next=s;
       r=s;

       s=(SDT)malloc(sizeof(sdt));
       strcpy(s->equipid,"P");
       s->type='3';
       s->dct=p;

       r->next=s;
       r=s;

       s=(SDT)malloc(sizeof(sdt));
       strcpy(s->equipid,"T");
       s->type='4';
       s->dct=t;

       r->next=s;
       r=s;

    r->next=NULL;

}



void init()
{
 //初始化系统设备表
init_creat_SDT(&SDT_table);

//两个通道的数据项初始化
// h1->chctnext=NULL;
 strcpy(h1->chanid,"通道1");
 h1->state=0;
 h1->useing=NULL;

// h2->chctnext=NULL;
 strcpy(h2->chanid,"通道2");
 h2->state=0;
 h2->useing=NULL;


//三个控制器数据项的初始化
 //c1->coctnext=NULL;
 strcpy(c1->controid,"控制器1");
 c1->state=0;
 c1->direct=h1;
 c1->useing=NULL;

 //c2->coctnext=NULL;
 strcpy(c2->controid,"控制器2");
 c2->state=0;
 c2->direct=h2;
 c2->useing=NULL;

 //c3->coctnext=NULL;
 strcpy(c3->controid,"控制器3");
 c3->state=0;
 c3->direct=h2;
 c3->useing=NULL;

 //各个设备数据项的初始化
 //k->dctnext=NULL;
 strcpy(k->equipid,"K");
 k->state=0;      //可用
 k->type='1';
 k->direct=c1;
 k->useing=NULL;

 //m->dctnext=NULL;
 strcpy(m->equipid,"M");
 m->state=0;
 m->type='2';
 m->direct=c1;
 m->useing=NULL;

 //p->dctnext=NULL;
 strcpy(p->equipid,"P");
 p->state=0;
 p->type='3';
 p->direct=c2;
 p->useing=NULL;

 //t->dctnext=NULL;
 strcpy(t->equipid,"T");
 t->state=0;
 t->type='4';
 t->direct=c3;
 t->useing=NULL;

}


void Insert(SDT head,char Name[10],DCT o);
void add()
{
    printf("添加的设备名:");
    scanf("%s",&Name);
 DCT o=(DCT)malloc(sizeof(Dct));
 Insert(SDT_table,Name,o);
 strcpy(o->equipid,Name);
 o->state=0;
 //printf("输入设备类型：");
 //scanf("%d\n",&o->state);
 o->type='5';
 o->useing=NULL;
 printf("input tempco:");
 scanf("%s",&tempco);
 if(!strcmp(tempco,"c1"))
    o->direct=c1;
 else if(!strcmp(tempco,"c2"))
    o->direct=c2;
 else if(!strcmp(tempco,"c3"))
    o->direct=c3;
    else
    {
        COCT co=(COCT)malloc(sizeof(coct));
        o->direct=co;
        strcpy(co->controid,tempco);
        co->state=0;

            printf("input tempch:");
        scanf("%s",&tempch);
        if(!strcmp(tempch,"h1"))
           co->direct=h1;
           else
           {
               co->direct=h2;
           }
    }

}

void dele1_sdt()
{
    SDT s,r;
    char Val[10];
    SDT head=SDT_table;
    printf("输入想要删除的设备：");
    scanf("%s",&Val);
    s=head->next;
    SDT m;
    m=head;
    while(s&&strcmp(s->equipid,Val))
    {
        s=s->next;
        m=m->next;

    }
    //printf("%s",m->equipid);

    //if(!(s->next)||strcpy(Val,s->equipid)!=0)

        //printf("没有此设备ll！\n");
    //else{
    r=m->next;
    m->next=r->next;
    printf("%s已经被删除\n",r->equipid);
    free(r);//}



}

void Insert(SDT head,char Name[10],DCT o)
{
    SDT s;
    s=head;
    while(s->next!=NULL)
    {
        s=s->next;
    }
   SDT r=(SDT)malloc(sizeof(sdt));
    strcpy(r->equipid,Name);
    r->type='5';
    r->dct=o;
    s->next=r;
    s=r;
    s->next=NULL;

}


void display_sdt()
{
    SDT r;
    CHCT p;
    COCT q;
    DCT m;
    r=SDT_table->next;
    while(r!=NULL)
    {

      printf("%s设备 %c 类型：",r->equipid,r->type);
      m=r->dct;
      printf("状态：%d",m->state);
      q=m->direct;
      printf("%s 状态：%d",q->controid,q->state);
      p=q->direct;
      printf("%s 状态：%d\n",p->chanid,p->state);
      r=r->next;
    }
}

DCT getequip(SDT head,char Val[10])
{
    SDT p;
    p=head->next;
    while(p&&(strcmp(Val,p->equipid)))
    {
        p=p->next;
    }
    if(!p||(strcmp(p->equipid,Val)))
    {printf("没有此设备!");}
    else
        {
            return p->dct;
        }
}

 void Insert_chct(CHCT head,PCB node)
 {

      PCB s=(PCB)malloc(sizeof(pcb));
       if(!s);
       s=node;
       s->Next=NULL;
       if(head->chctr==NULL)
       {
             head->chctf=s;
             head->chctr=s;
       }
       else
       {
         head->chctr->Next=s;
         head->chctr=s;
       }


 }

  void Insert_coct(COCT head,PCB node)
 {

      PCB s=(PCB)malloc(sizeof(pcb));
       if(!s);
       s=node;
       s->Next=NULL;
       if(head->coctr==NULL)
       {
             head->coctf=s;
             head->coctr=s;
       }
       else
       {
         head->coctr->Next=s;
         head->coctr=s;
       }


 }

  void Insert_dct(DCT head,PCB node)
 {

      PCB s=(PCB)malloc(sizeof(pcb));
       if(!s);
       s=node;
       s->Next=NULL;
       if(head->dctr==NULL)
       {
             head->dctf=s;
             head->dctr=s;
       }
       else
       {
         head->dctr->Next=s;
         head->dctr=s;
       }


 }

 PCB Delet_chct(CHCT head)
{
      PCB p,temp;
      if(head->chctf==NULL)
      {
        printf("this queue is NULL!\n");
         return NULL;
      }
      else
      {
        p=head->chctf;//将要删除的队列头节点暂存给p
        temp=p; //保存节点
        head->chctf=p->Next;     //将原对头节点后继p->Next赋值给头节点后继
        if(head->chctr==p)       //若对头是队尾，则删除后将rea指向头节点
         head->chctr=head->chctf;
         free(p);
         return temp;
      }

}

PCB Delet_coct(COCT head)
{
      PCB p,temp;
      if(head->coctf==NULL)
      {
        printf("this queue is NULL!\n");
         return NULL;
      }
      else
      {
        p=head->coctf;//将要删除的队列头节点暂存给p
        temp=p; //保存节点
        head->coctf=p->Next;     //将原对头节点后继p->Next赋值给头节点后继
        if(head->coctr==p)       //若对头是队尾，则删除后将rea指向头节点
         head->coctr=head->coctf;
         free(p);
         return temp;
      }

}

PCB Delet_Dct(DCT head)
{
      PCB p,temp;
      if(head->dctf==NULL)
      {
        printf("this queue is NULL!\n");
         return NULL;
      }
      else
      {
        p=head->dctf;//将要删除的队列头节点暂存给p
        temp=p; //保存节点
        head->dctf=p->Next;     //将原对头节点后继p->Next赋值给头节点后继
        if(head->dctr==p)       //若对头是队尾，则删除后将rea指向头节点
         head->dctr=head->dctr;
         free(p);
         return temp;
      }

}

void Creat_process()
{
    COCT r;
    CHCT s;
    PCB p=(PCB)malloc(sizeof(pcb));
    printf("输入进程名和所要申请的设备名");
    scanf("%s %s",p->id,p->equip);
    DCT q=getequip(SDT_table,p->equip);
    if(q->state==0)
    {
        printf("设备%s没有被使用\n",q->equipid);
        q->state=1;
        r=q->direct;
        if(r->state==0)
        {
            printf("%s没有被使用\n",r->controid);
            r->state=1;
            s=r->direct;
            if(s->state==0)
            {
                printf("%s没有被使用\n",s->chanid);
                s->state=1;
                Insert_chct(&chct_run,p);
                printf("%s进程正在运行状态\n",p->id);
            }
            else
            {
              Insert_chct(&chct_block,p);
              printf("%s",s->chanid);
              printf("通道正忙，进程被阻塞在通道下\n");
            }

        }
        else
        {
            Insert_coct(&coct_block,p);
            printf("%s",r->controid);
            printf("正忙，进程被阻塞在控制器下\n");
        }
    }
    else
    {
        Insert_dct(&Dct_block,p);
        printf("%s",q->equipid);
        printf("设备正忙，进程被阻塞在设备下\n");
    }
}

void end_process()
{
   PCB p;
   COCT r;
   CHCT s;
   p=Delet_chct(&chct_run);
   DCT q=getequip(SDT_table,p->equip);
     q->state=0;
     r=q->direct;
     r->state=0;
     s=r->direct;
     s->state=0;
p=Delet_chct(&chct_block);
        if(p!=NULL)
        {
          DCT q=getequip(SDT_table,p->equip);
          r=q->direct;
          s=r->direct;
         if(q->state==1&&r->state==1)
         {
             if(s->state==0)
             {
                printf("%s没有被使用\n",s->chanid);
                s->state=1;
                Insert_chct(&chct_run,p);
                printf("%s进程正在运行状态\n",p->id);
            }
            else{Insert_chct(&chct_block,p);}
         }
         else{Insert_chct(&chct_block,p);}
        }
        else
         {
            ;
          }


        p=Delet_coct(&coct_block);
             if(p!=NULL)
              {
                DCT q=getequip(SDT_table,p->equip);
                r=q->direct;
                s=r->direct;
                if(q->state==1&&r->state==0)
                    {
                        r->state=1;
                        printf("%s未被使用\n",r->controid);
                        if(s->state==0)
                        {
                            s->state=1;
                            printf("%s未使用\n",s->chanid);
                            Insert_chct(&chct_run,p);
                            printf("%s进程正在运行状态\n",p->id);
                        }
                        else
                        {
                            Insert_chct(&chct_block,p);
                            printf("%s",s->chanid);
                            printf("通道正忙，进程被阻塞在通道下\n");
                        }

                    }
                    else{Insert_chct(&chct_block,p);}
                }
                else
                {
                    ;
                }

     p=Delet_Dct(&Dct_block);
             if(p!=NULL)
              {

                if(q->state==0)
                 {
                    printf("%s没有被使用\n",q->equipid);
                    q->state=1;
                    r=q->direct;
                    if(r->state==0)
                        {
                            printf("%s没有被使用\n",r->controid);
                            r->state=1;
                            s=r->direct;
                        if(s->state==0)
                            {
                                printf("%s没有被使用\n",s->chanid);
                                s->state=1;
                                Insert_chct(&chct_run,p);
                                printf("进程正在运行状态\n");
                            }
                           else
                                {
                                Insert_chct(&chct_block,p);
                                printf("%s",s->chanid);
                                printf("通道正忙，进程被阻塞在通道下\n");
                                }

                        }
                    else
                        {
                            Insert_coct(&coct_block,p);
                            printf("%s",r->controid);
                            printf("正忙，进程被阻塞在控制器下\n");
                        }
                }
                else
                    {
                        Insert_dct(&Dct_block,p);
                        printf("%s",q->equipid);
                        printf("设备正忙，进程被阻塞在设备下\n");
                    }
              }
              else
                {
                    ;
                }




}
int main()
{
    init();
    printf("设备初始化完成！\n");
    display_sdt();
    char commend;
    printf("输入你想要的操作：");
    for(;;)
    {
        scanf("%s",&commend);
        if(commend=='A')
        {
            add();
        }
        else if(commend=='D')
        {
            dele1_sdt();
        }
        else if(commend=='S')
        {
            Creat_process();
        }
        else if(commend=='H')
        {
            end_process();
        }
        else
        {
            break;
        }
    }


    return 0;


}


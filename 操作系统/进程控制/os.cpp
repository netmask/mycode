#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include <windows.h>
#define ERROR 0
#define OK 1
typedef struct pcb
{
      char Name[20];
      char State[10];   //状态
      int Runtime;   //需要时间
      int Requiry;    //需要资源
      struct pcb *Next; //下一个指针
}pcb;
typedef struct pcb *PCB;
typedef struct
 {
        PCB front,rear;
 }Queue,*LinkQueue;

Queue Run_state;     //运行状态
Queue Ready_state;     //就绪状态
Queue Block_state;     //阻塞状态

 void Display_process(PCB);

void  logon()                                            //登陆函数 使系统更显完善
{
 printf("\n\t\t\t  process dispatch !\n");
 printf("\t\t\t    2013-9-19\n");
 printf("\t\t\t    C-26-514\n");
 printf("\t\t\t    Ace_cm_liu\n");

}

 void help()
{
      printf("-----HELP\n");

      printf("COMMEND:   ");
      printf("'C' ");
      printf("--- Creat process! ");
      printf("Format : 'C' + process name");

      printf("\nCOMMEND:   ");
      printf("'R' ");
      printf("--- Display queue,queue have 3: Ready_state ,Block_state ,Run_state! ");


      printf("\ncommend:   ");
      printf("'S' ");
      printf("--- autocall! ");



      printf("\nCommend:   ");
      printf("'B' ");
      printf("--- ready to running! ");

      printf("\ncommend:   ");
      printf("'Z' ");
      printf("--- running to block! ");


      printf("\ncommend:   ");
      printf("'W' ");
      printf("---  block to ready! ");

      printf("\nCommend:   ");
      printf("'E' ");
      printf("--- exit process! ");


      printf("\ncommend:   ");
      printf("'Q' ");
      printf("--- quit! \n");

}

 void initQueue(LinkQueue head)
 {
       head->front=head->rear=NULL;
 }


 void InsertQueue(LinkQueue head,PCB node)
 {

      PCB s=(PCB)malloc(sizeof(pcb));
       if(!s);
       s=node;
       s->Next=NULL;
       if(head->rear==NULL)
       {
             head->front=s;
             head->rear=s;
       }
       else
       {
         head->rear->Next=s;
         head->rear=s;
       }


 }

PCB DeleteQueue(LinkQueue head,PCB temp)
{
      PCB p;
      if(head->front==NULL)
      {
        printf("this queue is NULL!\n");
         return NULL;
      }
      else
      {
        p=head->front;//将要删除的队列头节点暂存给p
        temp=p; //保存节点
        head->front=p->Next;     //将原对头节点后继p->Next赋值给头节点后继
        if(head->rear==p)       //若对头是队尾，则删除后将rea指向头节点
         head->rear=head->front;
         free(p);
         return temp;
      }

}



void ReadAll(LinkQueue head)
{     //getchar();
      PCB p=head->front;
      printf("--Name        State     Runtime    Requiry--\n");
      while(p!=NULL)
      {
            printf("%-4s       %-4s        %-4d          %-4d\n",p->Name,p->State,p->Runtime,p->Requiry);
            p=p->Next;
      }
}

int length(LinkQueue head)
{  //getchar();
    PCB p=head->front;
    int sum=0;
    while(p!=NULL)
    {
        p=p->Next;
        ++sum;
    }
    return sum;
}
void Display_process(PCB node)     //显示进程状态
{
      printf("this process name is :%s\n",node->Name);
      printf("this process state is :%s\n",node->State);
      printf("this process runtime:%ds\n",node->Runtime);
      printf("this process requiry:%d\n",node->Requiry);

}

void DispatchToBlock(PCB node)        //创建进程后进入阻塞状态
{
      if(node->Requiry)
      {
            strcpy(node->State,"block");
              InsertQueue(&Block_state,node);
      }
}

void DispatchToReady(PCB node)         //创建进程后进入就绪状态
{
       strcpy(node->State,"Ready");
             InsertQueue(&Ready_state,node);
      /*if((node->Requiry)==0)
      {
            strcpy(node->State,"Ready");
             InsertQueue(&Ready_state,node);
      }*/
}



void DispatchReadyToRun()              //从就绪状态进入执行状态
{
      LinkQueue q;
        PCB Temp,p;
        int s=length(&Run_state);
        if(s==0)
        {
      p=DeleteQueue(&Ready_state,Temp); //删除就绪状态中此时节点P
      if(p!=NULL)
      {
      printf("state of ready process will be running!");
      strcpy(p->State,"Running");              //将状态改为执行状态
      p->Runtime=0;
      InsertQueue(&Run_state,p);  //插入运行队列
      }}
      else{printf("cpu is busy!");}
}

void DispatchRunToBlock()              //从运行状态进入阻塞状态
{
      LinkQueue q;
        PCB Temp,p;
      p=DeleteQueue(&Run_state,Temp);//删除运行状态中此时节点P
      if(p!=NULL)
     {
      printf("this process is not finished,will be dispath to the block queue!!\n");
      strcpy(p->State,"Block");//将状态改为阻塞状态
      p->Requiry=1;
      InsertQueue(&Block_state,p);  //插入阻塞队列
     }
}

void DispatchBlockToReady()              //从阻塞状态进入就绪状态
{
      LinkQueue q;
        PCB Temp,p;
      p=DeleteQueue(&Block_state,Temp); //删除阻塞状态中此时节点P
      if(p!=NULL)
      {
      printf("this process is not finished,will be dispatch to the ready queue!!\n");
      strcpy(p->State,"Ready"); //将状态改为就绪状态
      p->Requiry=0;
      InsertQueue(&Ready_state,p);  //插入就绪队列
      }
}


void DispatchRunToExit()              //从就绪状态进入执行状态
{
      LinkQueue q;
        PCB Temp,p;
      p=DeleteQueue(&Run_state,Temp);//删除执行状态中此时节点P
      if(p!=NULL)
      printf("state of run process  will be shutdown!!\n");
}

void Create_process(char pro[10])
{
    getchar();
      int i;
      PCB p;
      char *name=NULL;
      name=pro;
      srand(time(0));

            p=(pcb*)malloc(sizeof(pcb));
            strcpy(p->Name,name);
            strcpy(p->State,"create");
            p->Runtime=rand()%10+1;           //给进程随机分配时间(Time>0)
            p->Requiry=rand()%2+0;            //给进程随机定义所需资源（资源数是0或1）

            Display_process(p);
            Sleep(1);
            printf("%s will be in the state of ready,waiting in to running! \n\n",p->Name);
            DispatchToReady(p);
             /*if(!(p->Requiry))
              {
                 printf("%s will be in the state of ready,waiting in to running! \n\n",p->Name);
                 DispatchToReady(p);
              }
               else
               {
                   printf("%s will be in the state of block,waiting the resource ready! \n\n",p->Name);
                  DispatchToBlock(p);
               }*/
}

void process_Run()
{
      LinkQueue p,q,r;
      PCB temp,t;
      p=&Ready_state;
     while(p->front!=NULL)
     {
          r=&Run_state;
          q=&Block_state;

          DispatchReadyToRun();
          DispatchRunToExit();

         if(q->front!=NULL)
         {
             DispatchBlockToReady();
         }
         else
         {
             printf("state of block is null\n");
         }

     }
     printf("process is over!\n");

}

int main(void)
{
  char ch,pro[20];
   logon();

 int i;
for(;;)
{   getchar();
    system("cls");
help();

  printf("please input your commend!\n");

   ch = getchar();
while(ch)
{
  if(ch=='h')
  {
      help();
  }
  if(ch=='c')
  {
      scanf("%s",&pro);
     Create_process(pro);
  }
 if(ch=='r')
  {
      ReadAll(&Ready_state);
      ReadAll(&Block_state);
      ReadAll(&Run_state);
      getchar();
       /*scanf("%s",&pro);
      char str1[]={"Ready_state"};
      char str2[]={"Block_state"};
      char str3[]={"Run_state"};

      if(strcmp(str1,pro)==0)
      {
          ReadAll(&Ready_state);
      }
      if(strcmp(str2,pro)==0)
      {
          ReadAll(&Block_state);
      }
       if(strcmp(str3,pro)==0)
      {
          ReadAll(&Run_state);
      }*/
  }
  if(ch=='s')
  {
      getchar();
     process_Run();
  }
  if(ch=='b')
  {   getchar();
      DispatchReadyToRun();
  }
  if(ch=='z')
  {
      getchar();
      DispatchRunToBlock();
  }
  if(ch=='w')
  {
      getchar();
      DispatchBlockToReady();
  }
  if(ch=='e')
  {
      getchar();
      DispatchRunToExit() ;
  }
  if(ch=='q')
  {
      exit(0);
  }
  break;

}}
  return 0;
}

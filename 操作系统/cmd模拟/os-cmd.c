#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#define M 5
#define N 20

int disktable[M][N];
int Fat[N];
int p=0;
char s[36];
char s1[36];
char s2[36];
char s3[36];
int count = 0;

/*fileinfo 存储当前目录下的文件信息*/
struct fileinfo {
    char createtime[30];
    int Size;
    char name[20];
    int block;
};

/*每一个文件夹拥有一个record记录，包含着当前文件夹中的文件夹和文件信息*/
struct record {
    int depth;
    char name[20];
    char createtime[30]; //createtime没用，删去亦可
    char pwd[63];
    struct record * topdir;
    struct record * dir[6];
    struct fileinfo * file[6];
};


void Copyright()
{
    printf("Welcome to use Command line Beta1\n");
    printf("You can input 'h' to see the man\n\n");
}

void InitDiskTable()
{
    int i = 0, j = 0;
    srand( (unsigned)time( NULL ) );

   /* 产生0,1随机数 */
    for(i = 0; i < M; i++) {
        for(j = 0; j < N; j++)
            disktable[i][j] = rand()%2;
    }

    for(i = 0; i < M; i++) {
        for(j = 0; j < N; j++) {
            if(disktable[i][j] == 0)
                count++;
        }
    }
}

void ShowDiskTable()
{
    int i, j;
    count = 0;

    printf("\n---------------Disk Table---------------");
   // printf("\n----------------------------------------");
    for(i = 0; i < M; i++) {
        printf("\n");
        for(j = 0; j < N; j++) {
            printf("%d ", disktable[i][j]);
            if(disktable[i][j] == 0)
                count++;
        }
    }
    printf("\n----------------------------------------\n");
    printf("Total = 100, Free = %d, Used = %d, %d%%us\n", count, 100-count, (100-count));
}

void help()
{
    printf("\n");
    printf("\tls\t Show the dir and file\n");
    printf("\tcd\t Move the directory\n");
    printf("\ttouch\t Create a file\n");
    printf("\tmkdir\t Create a directory\n");
    printf("\trm\t Delete a file\n");
    printf("\trm -r\t Delete a directory\n");
    printf("\tquit\t Exit the system\n");
}

struct record * InitHome()
{
   struct fileinfo * tmp =  (struct fileinfo *)malloc(sizeof(struct fileinfo));
    strcpy(tmp->name, "ll");
    struct record * home = (struct record *)malloc(sizeof(struct record));
    home->depth = 0;
    strcpy(home->name, "ace.cm.liu");
    strcpy(home->createtime, " ");
    strcpy(home->pwd, "~");
    home->topdir = home;
    home->dir[0] = home;
    home->dir[1] = home;
    home->dir[2] = NULL;
    home->dir[3] = NULL;
    home->dir[4] = NULL;
    home->dir[5] = NULL;
    home->file[0]=NULL;
    home->file[1]=NULL;
    home->file[2]=NULL;
    home->file[3]=NULL;
    home->file[4]=NULL;
    home->file[5]=NULL;
    return home;
}

struct record * cmd_ls(struct record * dir_now) {
    int i = 2;
    struct record * s = dir_now;

    printf(". .. ");
    for(i=2;i<=5;i++)
    {
        if(s->dir[i]!=NULL)
        {
            printf("[d]%s ", s->dir[i]->name);
        }
    }
    for(i=0;i<=5;i++)
    {
        if(s->file[i]!=NULL)
        {
             printf("%s ", s->file[i]->name);
        }
    }
   /*) while(s->dir[i] != NULL) {
        printf("[d]%s ", s->dir[i]->name);
        i++;
    }
    i=0;
    while(s->file[i]!=NULL){
        printf("%s ", s->file[i]->name);
        i++;
    }*/
    printf("\n");
    return s;
}

struct record * cmd_cd(struct record * dir_now) {
    int i = 2;
    struct record * s = dir_now;

    if(strcmp(s2, ".") == 0)
        ;
    else if(strcmp(s2, "..") == 0)
        dir_now = dir_now->topdir;
    else {
        while(s->dir[i] != NULL) {
            if(strcmp(s->dir[i]->name, s2) == 0) {
                dir_now = s->dir[i];
                return dir_now;
            }
            i++;
        }
        printf("No directory named '%s', you can use 'ls' to see the list\n", s2);
    }
    return dir_now;
}

struct record * cmd_touch(struct record * dir_now) {
    int i = 0, t = 0;
    struct record * s = dir_now;
    while(s->file[i] != NULL) {
        if(strcmp(s->file[i]->name, s2) == 0) {
            printf("The '%s' file already exists\n", s2);
            break;
        }
        i++;
    }

    struct fileinfo * tmp =  (struct fileinfo *)malloc(sizeof(struct fileinfo));
    strcpy(tmp->name, s2);

    printf("Please input the file size(0~%d) > ", count);
    scanf("%d", &t);
    getchar();

    tmp->Size = t;
    tmp->block=p;
    printf("%d",tmp->block);
    int ii,jj;

            for(ii=0;ii<M;ii++)
            {
                for(jj=0;jj<N;jj++)
                {
                    if(disktable[ii][jj]==0)
                    {
                        if(t!=0)
                        {
                            Fat[p]=ii*100+jj;
                            disktable[ii][jj]=1;
                            t=t-1;
                            p=p+1;
                        }
                        else{break;}
                    }

                }
            }







    s->file[i] = tmp;
    return s;
    }

struct record * cmd_mkdir(struct record * dir_now) {
    int i = 2;
    struct record * s = dir_now;

    while(s->dir[i] != NULL) {
        if(strcmp(s->dir[i]->name, s2) == 0) {
            printf("The '%s' directory already exists\n", s2);
            break;
        }
        i++;
    }

    struct record * tmp = (struct record *)malloc(sizeof(struct record));
    tmp->depth = s->depth + 1;
    strcpy(tmp->name, s2);
    strcpy(tmp->createtime, " ");
    /*将文件名接在pwd后面*/
    char t[63];
    strcpy(t, s->pwd);
    strcat(t, "/");
    strcat(t, s2);
    strcpy(tmp->pwd, t);

    tmp->topdir = s;
    s->dir[i] = tmp;
    tmp->dir[2]=NULL;
    tmp->dir[3]=NULL;
    tmp->dir[4]=NULL;
    tmp->dir[5]=NULL;
    tmp->file[0]=NULL;
    tmp->file[1]=NULL;
    tmp->file[2]=NULL;
    tmp->file[3]=NULL;
    tmp->file[4]=NULL;
    tmp->file[5]=NULL;



    return s;
}

struct record * cmd_rm(struct record * dir_now) {
    struct record * s = dir_now;
    int i=0,p,ii,jj,start,ij;
    while(i!=6)
    {   if(s->file[i]!=NULL)
        {
        if(strcmp(s->file[i]->name,s2)==0)
        {
            for(p=0;p<s->file[i]->Size;p++)
            {
                start=(s->file[i]->block)+p;
                ij=Fat[start];
                ii=ij/100;
                jj=ij-ii*100;
                disktable[ii][jj]=0;
            }

            free(s->file[i]);
            s->file[i]=NULL;
            printf("rm!\n");
        }
        }
        i++;
    }
    return s;
}

struct record * cmd_rmdir(struct record * dir_now) {
    int i = 2,q,p,start,ij,ii,jj;
    struct record * s = dir_now;
    while(i!=6)
    {
       if(s->dir[i] != NULL)
        {
            if(strcmp(s->dir[i]->name,s3)==0)//文件夹在
            {
                for(q=0;q<=5;q++)//寻找文件夹中的文件
                {
                    if(s->dir[i]->file[q]!=NULL)//文件在
                    {
                        for(p=0;p<s->dir[i]->file[q]->Size;p++)//文件所占用的大小
                            {
                                start=(s->dir[i]->file[q]->block)+p;//
                                ij=Fat[start];
                                ii=ij/100;
                                jj=ij-ii*100;
                                disktable[ii][jj]=0;
                            }
                    }
                }


                free(s->dir[i]);
                s->dir[i] = NULL;
                printf("rmdir!\n");
            }
        }
        i++;
    }


    return s;
}


int GetCommand(struct record * dir_now)
{
    char buf[36];
    fgets(s ,36, stdin);

    strcpy(buf, s);

    /*strtok()函数用于把输入的字符串分割*/
    char* token = strtok(buf, " ");

    int i = 1;
    /*根据strtok()性质循环分割并分别保存到s1,s2,s3中*/
    while(token != NULL)
    {
        if(i == 1) {
            i++;
            strcpy(s1, token);
        }
        else if(i == 2) {
            i++;
            strcpy(s2, token);
        }
        else if(i == 3) {
            i++;
            strcpy(s3, token);
        }
        //printf("%s\n", token);
        token = strtok(NULL, " ");
    }

    /*i-1 为输入的参数个数，例：ls -l参数个数为2*/
    i = i-1;
    int len;

    if(i == 1) {

        /*因为分割后的字符串结尾会有回车符号，影响strcmp比较，所以测量长度，并把回车换为结束符*/
        len = strlen(s1);
        s1[len-1] = '\0';
        if(strcmp(s1, "h") == 0 || strcmp(s1, "help") == 0)
            return 0;
        else if(strcmp(s1, "ls") == 0)
            return 1;
        else if(strcmp(s1, "disk") == 0)
            return 7;
        else if(strcmp(s1, "q") == 0 || strcmp(s1, "quit") == 0)
            return 8;
        else
            return 9;
    }
    else if(i == 2) {
        len = strlen(s2);
        s2[len-1] = '\0';

        if(strcmp(s1, "cd") == 0)
            return 2;
        else if(strcmp(s1, "touch") == 0)
            return 3;
        else if(strcmp(s1, "mkdir") == 0)
            return 4;
        else if(strcmp(s1, "rm") == 0)
            return 5;
        else
            return 9;
    }
    else if(i == 3) {
        len = strlen(s3);
        s3[len-1] = '\0';
        if(strcmp(s1, "rm") == 0 && strcmp(s2, "-r")==0)
            return 6;
        else
            return 9;
    }
    else {
        printf("Error Input!\n");
        exit(-1);
    }
}

int main(void)
{
    int cmd;
    Copyright();
    InitDiskTable();

    struct record * home = InitHome();
    struct record * dir_now = home;

    while(1) {
        printf("ace.cm.liu@local_host:%s$ ", dir_now->pwd);
        cmd = GetCommand(dir_now);

        switch(cmd) {
            case 0: help(); break;
            case 1: dir_now = cmd_ls(dir_now); break;
            case 2: dir_now = cmd_cd(dir_now); break;
            case 3: dir_now = cmd_touch(dir_now); break;
            case 4: dir_now = cmd_mkdir(dir_now); break;
            case 5: dir_now = cmd_rm(dir_now); break;
            case 6: dir_now = cmd_rmdir(dir_now); break;
            case 7: ShowDiskTable(); break;
            case 8: exit(0); break;
            case 9: break;
        }
    }

    return 0;
}

#include<iostream>
#include<memory.h>
#include<string.h>
#include<iomanip>
using namespace std;
double data[6][20];
char ProName[20][10];
int num;
void show()
{
    char Firstcow[6][20]={ "到达时间", "服务时间", "开始时间", "完成时间","       T","       W" };
    cout<<"          ";
    for(int i=0;i<num;i++)
    {
        cout<<setw(5)<<ProName[i];
    }
    cout<<endl;
    for(int i=0;i<6;i++)
    {
        cout<<setiosflags(ios_base::right)<<Firstcow[i]<<":";
        for(int j=0;j<num;j++)
        {
            cout<<setiosflags(ios_base::right)<<setw(5)<<setprecision(2)<<data[i][j];
        }
        cout<<endl;
    }
}
void FCFS()
{
    bool flag[num];
    memset(flag,true,sizeof(flag));
    double tempFinishTime=-1.0;
    char Sequece[num][20];
    int order=0;
    for(int i=0;i<num;i++)
    {
        double min=100000.0;
        int pos=0;
        for(int j=0;j<num;j++)
        {
            if(flag[j]&&min>data[0][j])
            {
                min=data[0][j];
                pos=j;
            }
        }
        strcpy(Sequece[order++],ProName[pos]);
        if(data[0][pos]>tempFinishTime)
        {
            data[2][pos]=data[0][pos];
        }
        else
        {
            data[2][pos]=tempFinishTime;
        }
        data[3][pos]=data[2][pos]+data[1][pos];
        tempFinishTime=data[3][pos];
        flag[pos]=false;
    }
    for(int i=0;i<num;i++)
    {
        data[4][i]=data[3][i]-data[0][i];
        data[5][i]=data[4][i]/data[1][i];
    }
    show();
    cout<<num<<"个进程完成顺序:"<<endl;
    for(int i=0;i<num;i++)
        cout<<Sequece[i]<<ends;
    cout<<endl;
    double AverageT=0,AverageW=0;
        for(int i=0;i<num;i++)
        {
            AverageT+=data[4][i];
            AverageW+=data[5][i];
        }
        AverageT/=num;
        AverageW/=num;
        cout<<"平均周转时间T:"<<setprecision(2)<<AverageT<<endl;
        cout<<"平均带权周转时间W:"<<setprecision(2)<<AverageW<<endl;
}
void SJF()
{
    bool flag[num];
    memset(flag,true,sizeof(flag));
    int pos=0;
    char Sequece[num][20];
    int order=0;
    double min=10000.0;
    double tempFinishTime=-1.0;
    for(int j=0;j<num;j++)
    {
        if(flag[j]&&min>data[0][j])
        {
            min=data[0][j];
            pos=j;
        }
    }
    min=10000.0;
    data[2][pos]=data[0][pos];
    data[3][pos]=data[1][pos]+data[2][pos];
    tempFinishTime=data[3][pos];
    strcpy(Sequece[order++],ProName[pos]);
    flag[pos]=false;
    for(int j=1;j<num;j++)
    {
        min=10000.0;
        double shortJob=10000.0;
        for(int k=0;k<num;k++)
        {
            if(flag[k]&&shortJob>data[1][k])
            {
                shortJob=data[1][k];
                pos=k;
            }
        }
        if(data[0][pos]<=tempFinishTime)
        {

            data[2][pos]=tempFinishTime;
            data[3][pos]=data[2][pos]+data[1][pos];
            tempFinishTime=data[3][pos];
            flag[pos]=false;
        }
        else
        {
            min=10000.0;
            for(int i=0;i<num;i++)
            {
                if(flag[i]&&min>data[0][i])
                {
                    min=data[0][i];
                    pos=i;
                }
            }
            double tempShortJob=data[1][pos];
            for(int i=0;i<num;i++)
            {
                if(flag[i]&&data[0][i]<=tempFinishTime&&data[1][i]<data[1][pos])
                {
                    pos=i;
                }
            }
            if(tempFinishTime>=data[0][pos])
            {
                data[2][pos]=tempFinishTime;
                data[3][pos]=data[2][pos]+data[1][pos];
                tempFinishTime=data[3][pos];
                flag[pos]=false;
            }
            else
            {
                data[2][pos]=data[0][pos];
                data[3][pos]=data[2][pos]+data[1][pos];
                tempFinishTime=data[3][pos];
                flag[pos]=false;
            }
        }
        strcpy(Sequece[order++],ProName[pos]);
    }
    for(int i=0;i<num;i++)
    {
        data[4][i]=data[3][i]-data[0][i];
        data[5][i]=data[4][i]/data[1][i];
    }
    show();
    cout<<num<<"个进程完成顺序:"<<endl;
    for(int i=0;i<num;i++)
        cout<<Sequece[i]<<ends;
    cout<<endl;
    double AverageT=0,AverageW=0;
        for(int i=0;i<num;i++)
        {
            AverageT+=data[4][i];
            AverageW+=data[5][i];
        }
        AverageT/=num;
        AverageW/=num;
        cout<<"平均周转时间T:"<<setprecision(2)<<AverageT<<endl;
        cout<<"平均带权周转时间W:"<<setprecision(2)<<AverageW<<endl;
}
int main()
{
    cout<<"\t\t调度算法:"<<endl;
    cout<<"输入进程数num: ";
    cin>>num;
    cout<<"输入"<<num<<"个进程名"<<endl;
    for(int i=0;i<num;i++)
    {
        cin>>ProName[i];
    }
    cout<<"输入"<<num<<"个进程的各自到达时间"<<endl;
    for(int i=0;i<num;i++)
    {
        cin>>data[0][i];
    }
    cout<<"输入"<<num<<"个进程的各自服务时间"<<endl;
    for(int i=0;i<num;i++)
    {
        cin>>data[1][i];
    }
    int choose;
    cout<<"1.先来先服务(FCFS)"<<"    2.短作业优先(SJF)"<<endl;
    cout<<"输入你的选择:"<<ends;
    while(cin>>choose)
    {
        switch(choose)
        {
            case 1:FCFS();break;
            case 2:SJF();break;
            default:cout<<"ERROR!!!"<<endl;break;
        }
        cout<<"输入你的选择:"<<ends;
    }
    return 0;
}

#include<iostream>
#include<string.h>
#include<memory.h>
#include<iomanip>
#include<stdlib.h>
using namespace std;
#define M 5
#define N 3

int Max[M][N] =        {{7, 5, 3}, {3, 2, 2}, {9, 0, 2}, {2, 2, 2}, {4, 3, 3}};
int Allocation[M][N] = {{0, 1, 0}, {2, 0, 0}, {3, 0, 2}, {2, 1, 1}, {0, 0, 2}};
int Need[M][N] =       {{7, 4, 3}, {1, 2, 2}, {6, 0, 0}, {0, 1, 1}, {4, 3, 1}};
int Available[N] = {3, 3, 2};

char ProName[10][20];
char SouName[10][20];
char Sequeue[10][20];
int Request[10];
int SourceNum[10];
int Work[10];
bool Finish[10];
int n,m;

void available()
{   cout<<"可用资源："<<endl;
    for(int i=0;i<N;i++)
        cout<<Available[i]<<ends;
        cout<<endl;
}
void countSource()
{
    cout<<m<<"类资源总数量为:"<<endl;
    for(int i=0;i<m;i++)
    {   int temp=0;
        for(int j=0;j<n;j++)
        {
            temp+=Allocation[j][i];
        }
        SourceNum[i]=temp+Available[i];
        cout<<SourceNum[i]<<ends;
    }
    cout<<endl;
    return;
}
void countNeed(){
    cout<<"需求矩阵Need"<<"["<<n<<"]"<<"["<<m<<"]"<<":"<<endl;
    for(int i=0;i<n;i++)
    {
      for(int j=0;j<m;j++)
      {
          Need[i][j]=Max[i][j]-Allocation[i][j];
          cout<<Need[i][j]<<ends;
      }
      cout<<endl;
    }
     return ;
}
void allocation(){
    cout<<"分配矩阵Need"<<"["<<n<<"]"<<"["<<m<<"]"<<":"<<endl;
    for(int i=0;i<n;i++)
    {
      for(int j=0;j<m;j++)
      {
          cout<<Allocation[i][j]<<ends;
      }
      cout<<endl;
    }
     return ;
}
int safetyAlgorithm(){
    for(int i=0;i<m;i++)
      Work[i]=Available[i];
    memset(Finish,false,sizeof(Finish));
    int pos=0,order=0;
    int num=n,looptime=10*n;
   while(num)
   {
    if(Finish[pos]==false)
    {
        bool ok=true;
        for(int j=0;j<m;j++)
        {
            if(Need[pos][j]>Work[j])
            {
                ok=false;break;
            }
        }
        if(ok==true)
        {
            for(int j=0;j<m;j++)
            {
                Work[j]=Work[j]+Allocation[pos][j];
            }
            Finish[pos]=true;
            strcpy(Sequeue[order++],ProName[pos]);
             int count=0;
            for(int j=0;j<n;j++)
            {
                if(Finish[j]==true)
                {
                    count++;
                }
            }
            num--;
            if(count==n) return 1;
        }
        else
        {
            looptime--;
            int count=0;
            for(int j=0;j<n;j++)
            {
                if(Finish[j]==true)
                {
                    count++;
                }
            }
            if(looptime==0) return 0;
            if(count==n) return 1;
        }
    }
    pos=(pos+1)%n;
    if(Finish[pos])
    {
        do
        {
            pos=(pos+1)%n;
        }while(Finish[pos]);
    }
   }
    return 0;
}
int ProNameLogical(char rename[])
{
    for(int i=0;i<n;i++)
    {
        if(strcmp(rename,ProName[i])==0)
          return 1;
    }
    return 0;
}
void AllocationAlgorithm(char rename[])
{
    int pos=0,sum=0;
    for(int i=0;i<n;i++)
    {
        if(strcmp(rename,ProName[i])==0)
        {
            pos=i;
            break;
        }
    }
    //cout<<"当前进程"<<ProName[pos]<<" pos:"<<pos<<endl;
    for(int j=0;j<m;j++)
    {
        if(Request[j]>Need[pos][j])
        {
            cout<<"所需要的资源数超过最大值，重新申请"<<endl;
            return  ;
        }
    }
    for(int j=0;j<m;j++)
    {
         if(Request[j]>Available[j])
         {
            cout<<"尚无足够的资源,"<<rename<<"需等待"<<endl;
            return ;
        }
    }
            int TempAvailable[m],TempAllocation[m],TempNeed[m];
            for(int j=0;j<m;j++)
            {
                TempAvailable[j]=Available[j];
                TempAllocation[j]=TempAllocation[j];
                TempNeed[j]=Need[pos][j];
            }
            for(int j=0;j<m;j++)
            {
                Available[j]=Available[j]-Request[j];
                Allocation[pos][j]=Allocation[pos][j]+Request[j];
                Need[pos][j]=Need[pos][j]-Request[j];
            }
            if(safetyAlgorithm())
            {
                cout<<"可以分配,安全序列:"<<endl;
                for(int i=0;i<n;i++)
                   cout<<Sequeue[i]<<ends;
                    cout<<endl;
                for(int j=0;j<m;j++)
                {
                   if(Need[pos][j]==0)
                   {
                       sum=sum+1;
                   }
                   else{break;}
                }
                if(sum==m)
                {
                    for(int i=0;i<m;i++)
                    {
                        Available[i]=Available[i]+ Allocation[pos][i];
                        Allocation[pos][i]=0;
                        Max[pos][i]=0;

                    }
                    cout<<"成功收回进程"<<rename<<"所分配资源"<<endl;
                }
                else;
                 available();
                allocation();
                cout<<endl;
                countNeed();
                cout<<endl;


            }
            else
            {
                cout<<"不安全，不可以分配,进程"<<rename<<"需等待"<<endl;
                for(int j=0;j<m;j++)
                {
                    Available[j]=TempAvailable[j];
                    TempAllocation[j]=TempAllocation[j];
                    Need[pos][j]=TempNeed[j];
                }
            }
}
int main()
{
    system("title 银行家算法");

    n = 5;
    strcpy(ProName[0], "P0");
    strcpy(ProName[1], "P1");
    strcpy(ProName[2], "P2");
    strcpy(ProName[3], "P3");
    strcpy(ProName[4], "P4");
    m = 3;
    strcpy(SouName[0], "A");
    strcpy(SouName[1], "B");
    strcpy(SouName[2], "C");

    countSource();
    available();
    allocation();
    cout<<endl;
    countNeed();

    int safe=safetyAlgorithm();
    if(safe)
    {
        cout<<"安全的,存在安全序列:"<<endl;
        for(int i=0;i<n;i++)
            cout<<Sequeue[i]<<ends;
        cout<<endl;
    }
    else
    {
        cout<<"不安全的"<<endl;
        return 0;
    }

    while(1)
    {
        char rename[20];
        cout<<"输入要申请资源的进程名: ";
        cin>>rename;
        if(!ProNameLogical(rename))
        {
            cout<<"进程名不合法,重新输入:";
            do cin>>rename;
            while(ProNameLogical(rename));
        }
        cout<<"输入进程"<<rename<<"要申请的"<<m<<"类资源的值:"<<endl;
        for(int i=0;i<m;i++)
            cin>>Request[i];
        AllocationAlgorithm(rename);

    }

  return 0;
}

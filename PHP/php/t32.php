<?php
/*
#for循环必须是下标连续的菜能遍历，在php中一般采用foreach($var1 as $var2=>$var3)遍历
#挨个遍历var1数组将值保存在var3中，下标保存在var2中
#next();数组指针下一个
#prev();数组指针上一个
#reset();数组指针重新回到第一个
#end();数组指针最后一个
#current();获取当前元素
#key();获取当前元素的键值
/
$info=array(
	"user"=>array(
					array(1,"zhangsan","nan"),
					array(2,"lisi","nv"),
					array(3,"wawu","nv")
				),
	"score"=>array(
					array(1,"php",80),
					array(2,"java",90),
					array(3,".net",100)
	),
	"email"=>array(
					array(1,"php@.com",180),
					array(2,"java@.com",190),
					array(3,".net@.com",100)
	)
);

foreach($info as $tablename=>$table){
		echo '<table align="center" border="1" width="500">';
		echo '<caption><h1>'.$tablename.'</h1></caption>>';
		foreach($table as $row){
			echo '<tr>';
			foreach($row as $col){
				echo '<td>'.$col.'<td>';
				}
			echo '</tr>';
			}
		
		echo '</table>';
		
	};
	*/

	$use=array("id"=>1,"name"=>"zhansan","age"=>"nan");
	
	while(list($key , $value)=each($use))
	{
		echo "sd";
		echo $key."==>".$value."<br>";
		}
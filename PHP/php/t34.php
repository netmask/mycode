<?php
/*数组的处理函数
* 一 数组键/值操作的有关函数
* 1 array_values();  将数组从关联索引变为顺序索引
* 2 array_keys();     读取键值
* 3 in_array();    判断数组中的值是够存在
* 4 array_key_exists(); 判断键值是否存在
* 5 array_flip();   将键值与值对调
* 6 array_reverse();  顺序反转
*
* 二 统计数组元素中的个数和唯一性
* 1 count() sizeof();
* 2 array_count_values();  统计数组中所有值出现的次数
* 3 array_unique();   移除数组中重名的值
*
* 三 使用回调函数处理数组的函数
* 1 array_filter();用回调函数过滤数组中的单元
* 2 array_walk();将数组中的每个成员应用到自己所定义的函数
* 3 array_map();将回调函数作用到给定的数组单元上
*/

//  														测试数组一
  $lamp=array("os"=>"linux","web"=>"apache","db"=>"mysql","language"=>"lamp","html"=>100,"os1"=>"linux");
  $lamp2=array("os"=>"windows","web"=>"apache","db"=>"oracle","language"=>"lamp","html"=>100,"os1"=>"linux");
//*/

/*
  $lamp=array(1,2,4,-3,5,6,-5,3,-4,56,-6,7,-7,8,9,10,0);
*/ 
/*  测试array_values();
$arr=array_values($lamp);
list($os,$a,$b,$c,$d)=$arr;
echo $os;
*/

/*  测试array_keys();
$arr=array_keys($lamp);
$arr=array_keys($lamp,100,true);
*/

/*   测试in_array();
if(in_array("linux",$lamp))
{
	echo "exists!";
	}
	else{
		echo " is not exists!";
		}
*/

/*  测试键值是否存在
if(array_key_exists("os",$lamp))
{
	echo "exists!";
	}
	else{
		echo " is not exists!";
		}
*/

/*  
测试array_flip();切换函数
$arr=array_flip($lamp);   //当值有相同的键值不同时在调换的时候因为规则下标不能够相同因此调换时会将后面的键值覆盖前面的键值
*/

/*
$arr=array_reverse($lamp);
*/


/*
echo count($lamp);
echo sizeof($lamp);
*/
/*
$arr=array_count_values($lamp);
$arr=array_unique($lamp);
*/

/* 测试 过滤函数
$arr=array_filter($lamp,"fun");
function fun($n){
		if($n%2==0){
			return true;
			}
			else{
				return false;
				}
	}
*/	
/* 测试应用函数
array_walk($lamp,"fun");
/*function fun($value,$key){
	echo "tish key '$key' value is '$value'".'<br>';
	}
*/
/*function fun(&$value,$key){
	$value="web";
	}
*/
$arr=array_map("fun",$lamp,$lamp2);
function fun($lam1,$lam2){
	if($lam1==$lam2){
	return "same";
	}
	return "notsame";
}
/*function fun($n){
	echo "==".$n."==".'<br>';
	}
*/echo '<pre>';
print_r($arr);
echo '</pre>';
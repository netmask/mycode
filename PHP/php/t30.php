<?php
/*
#递归调用演示
#<?php                                                                                   #结果：9
#	$number=0;                                                                                 8
#	function demo($number)                                                                     7
#	{                                                                                          |
#		$number--;																			   0
#    echo $number.'<br>';                                                                      ---------------
#		if($number>0)																		   0
#		{																					   |
#			demo($number);																	   7
#		}																					   8
#		else																				   9
#			{
#				echo "-----------------".'<br>';
#			}
#		echo $number.'<br>';
#	    }
#	demo(10);
#
#
#include "filename";     include_once "filname";     动态包含
#include("filename")     include_once("filename")
#require "filename";     .....                       静态包含
#require("filename")     .....

例如：    require "top.inc.php";
		if($classname=="Action")
		{
			include "action/".$classname.".php";
		}
			elseif($classname=="model")
			{
				include "model/".$classname.".php";
			}
			else
			{
			 	include "type/".$classname.".php";	
			}
				
		require "footer.inc.php";
#
#读取dirname目录下的文件，查看目录数和文件数
#totle（）；传入3个参数，为初始化的目录数和文件数
#opendir();函数为打开函数，打开后的资源放入变量$dir
#readdir();函数为读取函数，读取当前路径下的文件
#closedir();关闭所打开的目录。
#通过while循环读取路径下文件下文件
#is_dir为读取当前路径下的文件即$filename下的，而我们需要知道的是$dirname.'/'.$filename
*/
include "demo.php";
demo(10);
	function totle($dirname,&$dirnum,&$filenum){
			$dir=opendir($dirname);
			echo readdir($dir).'<br>';
			echo readdir($dir).'<br>';
			while($filename=readdir($dir))
			{
				$newfile=$dirname.'/'.$filename;
				if(is_dir($newfile))
				{
					totle($newfile,&$dirnum,&$filenum);//函数递归调用，如果是目录则返回totle入口，对当前路径下目录再次进行遍历。
					$dirnum++;
					}
					else
					{
						$filenum++;
						}
				echo $filename.'<br>';
				}
				closedir($dir);
		}
		$dirnum=0;
		$filenum=0;
		totle("D:/lamp/Apache2.2/htdocs",$dirnum,$filenum);
		echo "The dir totle: ".$dirnum.'<br>';
		echo "The file totle: ".$filenum.'<br>';
<?php
/*
#数组申明：
#索引 下标从0-N  例如 array[0]
#关联 “字符串申明”    array["zhangsan"]
#数组中包含数组即为而为数组，同一数组可以存放任意类型值
#指定下标  “键”=>"值"   多个数组用“，”隔开
*/
$info=array(
	"user"=>array(
					array(1,"zhangsan","nan"),
					array(2,"lisi","nv"),
					array(3,"wawu","nv")
				),
	"score"=>array(
					array(1,"php",80),
					array(2,"java",90),
					array(3,".net",100)
	),
	"email"=>array(
					array(1,"php@.com",180),
					array(2,"java@.com",190),
					array(3,".net@.com",100)
	)
);
echo $info["email"][1][1];    //查找
echo '<pre>';
print_r($info);
echo '</pre>';
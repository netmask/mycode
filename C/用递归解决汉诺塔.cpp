#include<stdio.h>
#include<iostream>
using namespace std;

int m=0;//定义全局变量m用于计数move
void move (char A,int n,char C)
{
    m++;
    printf("step%d:Move%d,From %c To %c\n",m,n,A,C);

}

void Hanoi(int n,char A,char B,char C)//Hanoi 内部函数的实现
{
if(n==1)
move(A,1,C);   //将编号为1的圆盘从A移动到C
else
{
Hanoi(n-1,A,C,B);    //将A上编号为1至n-1的圆盘移到B，C做辅助塔座
move(A,n,C);         //将编号为N的圆盘从A移动到C
Hanoi(n-1,B,A,C);    //将B编号为1至n-1的圆盘移到C，A做辅助塔座
}
}

int main(void)
{
int n;
printf("请输入需要移动的盘子数：");
while(scanf("%d",&n)!=EOF)//遇到Ctrl+Z结束
Hanoi(n,'A','B','C');//有n个盘子从A借助B移动到C，当此函数结束，也就移完了
return 0;
}


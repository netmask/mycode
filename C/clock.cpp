#include "stdio.h"
#include "stdlib.h"
#include "time.h"

int main( void )
{
   long    i = 10000000;
   clock_t start, finish;
   double  duration;
   /* 测量一个事件持续的时间*/
   printf( "Time to do %ld empty loops is ", i );
   start = clock();//开始计时
   while( i-- )      ;
   finish = clock();//结束计时
   duration = (double)(finish - start) / CLOCKS_PER_SEC;
   printf( "%f seconds\n", duration );
   system("pause");
   return 0;
}

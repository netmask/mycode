#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>


  typedef struct Node{
  int data;
  struct Node * pNext;
  }NODE,*PNODE;  //NODE等价于struct Node,PNODE等价于struct Node *

  //函数声明
  PNODE create_list(void);
  void traverse_list(PNODE pHead);

  int length_list(PNODE pHead);

  bool delete_is(PNODE,int,int *);

  int main(void)
  {
      int val,pos;
    PNODE pHead;
    pHead = create_list();//create_list()创建一个非循环的单链表，并将链表的头节点付给pHead;
    traverse_list(pHead);//遍历所有的节点
    printf("即将进行删除操作，删除为pos的前一个节点，请输入pos=");
    scanf("%d",&pos);
     if(delete_is(pHead,pos,&val))
    {
        printf("删除成功,您删除的元素是=%d\n",val);
    }
    else
     printf("删除失败，您删除的元素不存在");
    traverse_list(pHead);
    return 0;
  }

  PNODE create_list(void)
  {
      int len;//用来存放有效节点的个数
      int val;//用来临时存放用户输入的节点的值
      int i;
      PNODE pHead=(PNODE)malloc(sizeof(NODE));

      PNODE pTail=pHead;


      //pTail->pNext=NULL;//当用户只输入0个节点时



      printf("请用户输入节点的长度len= ");
      scanf("%d",&len);
      for(i=0;i<len;i++)
    {
        printf("请用户输入节点的值",i+1);
        scanf("%d",&val);
        PNODE pNew=(PNODE)malloc(sizeof(NODE));

       pNew->data=val;
       pTail->pNext=pNew;
       pNew->pNext=pHead;
       pTail=pNew;
    }

  return (pHead);

  }

  void traverse_list(PNODE pHead)
  {
      PNODE p=pHead->pNext;
      int i;
      int len=length_list(pHead);
      for(i=0;i<len;i++)
      {
       printf("%d ",p->data);
       p=p->pNext;
      }
      printf("\n");
      return;
  }


int length_list(PNODE pHead)
{
    int len=0;
  PNODE p=pHead->pNext;
  while(p!=pHead)
  {
    ++len;
    p=p->pNext;
  }
  return len;
}





bool delete_is(PNODE pHead,int pos,int *pVal)
{

    PNODE S;
    PNODE q=pHead;
    while(q->pNext->data!=pos)
    {
       q=q->pNext;
    }
       S=q->pNext;
    PNODE p=S->pNext;

    while(p->pNext->pNext->data!=pos)
    {
        p=p->pNext;
    }
    *pVal=p->pNext->data;
    p->pNext=p->pNext->pNext;

    return true;
}

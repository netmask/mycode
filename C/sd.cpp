#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
struct Arr{
int *pBase;//存储数组的第一个单元
int len;//数组所能容纳的最大元素个数
int cnt;//当前数组有效元素个数
};
void init_arr(struct Arr *pArr,int length);//数组的初始化
bool append_arr(struct Arr *pArr,int val);
bool is_empty(struct Arr *pArr);//判断是否为空
bool is_full(struct Arr *pArr);//判断是否满
void show_arr(struct Arr *pArr);//显示数组


int main()
{
    struct Arr arr;
    init_arr(&arr,4);
    show_arr(&arr);

    printf("%d\n",arr.len);
    append_arr(&arr,1);
    append_arr(&arr,2);
    append_arr(&arr,2);
    append_arr(&arr,4);
    if(append_arr(&arr,5))
      printf("true\n");
      else
      printf("false\n");
    show_arr(&arr);

    return 0;
}

 bool is_empty(struct Arr *pArr)
{
 if(0==pArr->cnt)
     return true;
 else
     return false;
}

bool is_full(struct Arr *pArr)
{
  if (pArr->cnt==pArr->len)
      return true;
      else
       return false;
}

void show_arr(struct Arr *pArr)
{
    if(is_empty(pArr))
    {
      printf("数组为空\n");
    }
    else
    {
        for(int i=0;i<pArr->cnt;i++)
        printf("%d ",pArr->pBase[i]);
    }
}
void init_arr(struct Arr *pArr,int length)
{
    pArr->pBase=(int *)malloc(sizeof(int)*length);
    if(NULL==pArr->pBase)
    {printf("动态内存分配失败");
    exit(-1);
    }
    else
    {
        pArr->len=length;
        pArr->cnt=0;
    }

    return;
}

bool append_arr(struct Arr *pArr,int val)
{
    //满是否返回false
    if(is_full(pArr))
    return false;
    //不满时追加
    pArr->pBase[pArr->cnt]=val;
    (pArr->cnt)++;
    return true;
}

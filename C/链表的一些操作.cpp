#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>

  typedef struct Node{
  int data;
  struct Node * pNext;
  }NODE,*PNODE;  //NODE等价于struct Node,PNODE等价于struct Node *

  //函数声明
  PNODE create_list(void);
  void traverse_list(PNODE pHead);
  bool is_empty(PNODE pHead);
  int length_list(PNODE pHead);
  bool insert_list(PNODE,int,int);
  bool delete_is(PNODE,int,int *);
  void sort_list(PNODE);

  int main(void)
  {
      int val;
      int pos;
    PNODE pHead=NULL;
    pHead = create_list();//create_list()创建一个非循环的单链表，并将链表的头节点付给pHead;
    traverse_list(pHead);//遍历所有的节点
    printf("请输入您想要插入的位置（前插）和插入的值,pos,val ");
    scanf("%d%d",&pos,&val);
     insert_list(pHead,pos,val);
     traverse_list(pHead);
     printf("后删操作，您要删除第几个节点值");
     scanf("%d",&pos);
    if(delete_is(pHead,pos,&val))
    {
        printf("删除成功,您删除的元素是=%d\n",val);
    }
    else
     printf("删除失败，您删除的元素不存在");
    traverse_list(pHead);

    int len=length_list(pHead);
    printf("链表的长度是%d\n",len);

    sort_list(pHead);
    printf("数据排序：\n");
    traverse_list(pHead);

    if(is_empty(pHead))
    printf("链表为空\n");
    else
    printf("链表不为空\n");
    return 0;
  }

  PNODE create_list(void)
  {
      int len;//用来存放有效节点的个数
      int val;//用来临时存放用户输入的节点的值
      int i;
      PNODE pHead=(PNODE)malloc(sizeof(NODE));
      PNODE pTail=pHead;
      pTail->pNext=NULL;//当用户只输入0个节点时


      if(pHead==NULL)
      {
        printf("分配内存失败，程序终止！");
        exit(-1);
      }
      printf("请用户输入节点的长度len= ");
      scanf("%d",&len);
      for(i=0;i<len;i++)
    {
        printf("请用户输入节点的值",i+1);
        scanf("%d",&val);
        PNODE pNew=(PNODE)malloc(sizeof(NODE));
         if(pNew==NULL)
      {
        printf("分配内存失败，程序终止！");
        exit(-1);
      }
       pNew->data=val;
       pTail->pNext=pNew;
       pNew->pNext=NULL;
       pTail=pNew;
    }

  return (pHead);

  }

  void traverse_list(PNODE pHead)
  {
      PNODE p=pHead->pNext;
      while(p!=NULL)
      {
       printf("%d ",p->data);
       p=p->pNext;
      }
      printf("\n");
      return;
  }
 bool is_empty(PNODE pHead)
 {
     if(pHead->pNext==NULL)
     return true ;
     else
     return  false  ;

 }

int length_list(PNODE pHead)
{
    int len=0;
  PNODE p=pHead->pNext;
  while(p!=NULL)
  {
    ++len;
    p=p->pNext;
  }
  return len;
}

void sort_list(PNODE pHead)
{
 int i,j,t;                      //泛型：通过某种技巧达到相同的目的
 PNODE p,q;
 int len=length_list(pHead);
  for(i=0,p=pHead->pNext;i<len-1;p=p->pNext,i++)
  {
    for(j=i+1,q=p->pNext;j<len;q=q->pNext,j++)
    {
      if(p->data>q->data)     //类似于a[i]>a[j]
      {
          t=p->data;   //类似于a[i]
          p->data=q->data;   //类似于a[i]=a[j];
          q->data=t;    //类似于a[j]=t;
      }
    }
  }
  return ;
}
//表示在pos节点之前插入val
bool insert_list(PNODE pHead,int pos,int val)
{
    int i=0;
    PNODE p=pHead;

    while(p!=NULL && i<pos-1)
    {
      p=p->pNext;
      i++;
    }
    if(i>pos-1 || p==NULL)
    {
      return false;
    }
    PNODE pNew=(PNODE)malloc(sizeof(NODE));
    if(pNew==NULL)
    {
        printf("分配内存失败，程序终止！");
        exit(-1);
    }
    pNew->data=val;
    PNODE q=p->pNext;
    p->pNext=pNew;
    pNew->pNext=q;
    return true;
}

bool delete_is(PNODE pHead,int pos,int *pVal)
{
    int i=0;
    PNODE p=pHead;
    while(p->pNext!=NULL && i<pos-1)
    {
      p=p->pNext;
      i++;
    }
    if(i>pos-1 || p->pNext==NULL)
    {
      return false;
    }

    PNODE q=p->pNext;
    *pVal=q->data;
    //删除p节点后面的节点
    p->pNext=p->pNext->pNext;
    free(q);
    q=NULL;
    return true;
}

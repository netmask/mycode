来自郝斌老师数据结构
排序:
冒泡
插入：从头开始选择第2个按大小插入前2个之间，再将第3个按大小插入前俩个之间依次。。。
选择
快速排序
归并排序：俩俩排序，然后四四归并排，再八八排
#include<stdio.h>
int FindPos(int *a,int low,int high);
void QuickSort(int *a,int low,int high);
int main(void)
{
    int i;
int a[7]={5,2,6,8,4,3,7};
 QuickSort(a,0,6);
 printf("快排从小到大：");
 for(i=0;i<7;i++)
 printf("%d ",a[i]);
 printf("\n");
return 0;
}
void QuickSort(int *a,int low,int high)//快排通过确定一个值的位置，然后将其分为二半，
                                                       //分别进行再次确定位置
{
    int pos;
    if(low<high)     //采用递归
    {
     pos=FindPos(a,low,high);
     QuickSort(a,low,pos-1);
     QuickSort(a,pos+1,high);
     }

}
int FindPos(int *a,int low,int high)//确定位置
{
    int val=a[low];   //此时选取首地址所指向的值赋给即将要确定位置的变量
    while(low<high)
    {
        while(low<high&&a[high]>=val)//从high判断，若大于val则移动，反之则将此时high地址所指向的值
                                                      //赋给low所指向的值
          --high;
        a[low]=a[high];
        while(low<high&&a[low]<=val)//当赋值完毕后，则从赋值处开始比较，若此时low所指向的值小于val则向前移动，
                                    //反之，则将此时low地址所指向的值赋给high
          ++low;
        a[high]=a[low];
    }
    a[low]=val;//此时循环完毕high=low程序已经进行完确定位置，将val的值赋给确定的位置
    return low;  //当程序循环后，low=high所以也可以是return high;
}

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>


int main(void)
{
    struct timeval start;
    struct timeval end;

   long i=1000000;

    unsigned long diff_1;

    gettimeofday(&start, NULL); //gettimeofday()是C语言获得精确时间函数
    //需要及时的函数
    while(--i) ;
    //
    gettimeofday(&end, NULL);
    diff_1 = 1000000 * (end.tv_sec - start.tv_sec) + end.tv_usec - start.tv_usec;

    printf("time_1 = %ld\n", diff_1);
        return 0;
}

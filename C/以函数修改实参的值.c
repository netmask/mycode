#include<stdio.h>
void f(int *p);  //前向声明
int main(void)
{
  int i=9;
  printf("i的地址为=%p\n",i);
    printf("i=%d\n",i);
  f(&i);       //通过函数修改实参的值
    printf("i的地址变为%p\n",i);    //%p是以16位地址输出
    printf("i=%d\n",i);
  return 0;
}
void f(int *p)
{
    *p=99;
}

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define N 1000

using namespace std;

typedef struct
{
    char ch;
    int power;
    int parent,lchild,rchild;
}Huffman;
int  n;


void Huffman_get(Huffman *T,char *filename)
{
    FILE *fp;
    char ch;
    if((fp=fopen(filename,"r"))==NULL)
    {
    	cout<<"wrong"<<endl;
        exit(0);
    }
    int i;
    for(i = 0;i < N;i++)
    {
        T[i].ch = '0';
        T[i].power = 0;
        T[i].parent = 0;
        T[i].lchild = -1;
        T[i].rchild = -1;
    }
    while(!feof(fp))
    {
         ch = fgetc(fp);
      for(i = 1;T[i].power != 0;i++)
        {
          if(T[i].ch == ch){T[i].power++;break;}
        }
        if(T[i].power != 0)continue;
        else
          {
            T[i].ch = ch;
            T[i].power++;
            n = i;
          }
    }
     fclose(fp);
}



void Huffman_Select(Huffman *HT,int len,int &s1,int &s2)//选择两个最小的数
{

    int i,j,s = 0;
    int m;
    for(j=1;j<len;j++)
    {
        if(s == 2)break;
        if(HT[j].parent == 0)
        {
            m = HT[j].power;
           if(s == 0)s1 = j;
          else s2 = j;
           s++;
        }
        else continue;
      for(i = j+1;i < len;i++)
       if(m > HT[i].power&&HT[i].parent == 0)
        {
            m = HT[i].power;
           if(s == 1)s1 = i;
           if(s == 2) s2 = i;
        }
       HT[s1].parent = -1;
    }
}



void Huffman_Creat(Huffman *T)//创建HFM树
{
     int s1,s2;
     int i;
     for(i = n;i <= 2*n-3;++i)
     {
       Huffman_Select(T,i,s1,s2);
       T[s1].parent = T[s2].parent = i;
       T[i].lchild = s1;
       T[i].rchild = s2;
       T[i].power = T[s1].power + T[s2].power;
     }
}

 void Huffman_code(Huffman *HT,char *filename)//编码
 {
     char *cd;
     char **HC;
     char ch;
     FILE *fp;
     fp=fopen(filename,"r");
     HC = new char *[N];
     cd = new char [n];
     int i,j=1;
     int c,f,s;
     cd[n-1] = '\0';
     cout<<"file content:"<<endl;
     while(!feof(fp))
     {
         ch = fgetc(fp);
         cout<<ch;
        for(i = 1;i <= n;i++)
        {

           if(HT[i].ch == ch)
            {
              s = n-1;
              c = i;
              f = HT[i].parent;
            while(f != 0)
              {
                --s;
               if(HT[f].lchild == c)cd[s] = '0';
               else cd[s] = '1';
               c = f;f = HT[f].parent;
              }
              HC[j] = new char[n-s];
              strcpy(HC[j],&cd[s]);
              j++;
            }
        }
     }
     delete cd;
     printf("\n");
     printf("\n");

     printf("file one by one Creatcode:\n ");
      for(i = 1;i < n;i++)
       {
        printf("%c:%-10s   ",HT[i].ch,HC[i]);
        if(i%5 == 0)cout<<endl;
       }
       printf("\n");
       printf("\n");


     printf("file Creatcode:\n");

     for(i=1;i < j-1;i++)
     printf("%s ",HC[i]);
	 printf("\n");
	 printf("\n");
	 cout<<"input your save filename:\n";
	 char file_name[N];
	 scanf("%s",file_name);
	 FILE *fp2;
	 if((fp2=fopen(file_name,"w"))==NULL){
		 printf("fil elwronge\n");
		 exit(0);
	 }
     for(i=1;i < j-1;i++)
		 fputs(HC[i],fp2);
	 fclose(fp2);
 }

void Huffman_Decode(Huffman *HT,char *s)
{

	int i,m;
    m = 2*n-3;
    for(i = 0;i<strlen(s);i++)
    {
        if(s[i] == '0')m = HT[m].lchild;
        else m = HT[m].rchild;
        if(HT[m].lchild == -1&&HT[m].rchild ==-1)
        {
            cout<<HT[m].ch;
            m = 2*n-3;
        }

    }
    printf("\n");
}
int main()
{

    Huffman T[N];
    char filename[20];
     cout<<"input  Creatcode filename:"<<endl;
     scanf("%s",filename);
     cout<<endl;
     Huffman_get(T,filename);//读取文件，并设置权值
     Huffman_Creat(T);//创建HFM树
     Huffman_code(T,filename);    //编码
     char s[N];
     cout<<endl;
     cout<<"input  Decode Creatcode:"<<endl;
     scanf("%s",s);
      Huffman_Decode(T,s);   //译码

return 0;
}

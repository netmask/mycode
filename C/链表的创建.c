#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>

  typedef struct Node{
  int data;
  struct Node * pNext;
  }NODE,*PNODE;  //NODE等价于struct Node,PNODE等价于struct Node *

  //函数声明
  PNODE create_list(void);
  void traverse_list(PNODE pHead);

  int main(void)
  {
    PNODE pHead=NULL;
    pHead = create_list();//create_list()创建一个非循环的单链表，并将链表的头节点付给pHead;
    traverse_list(pHead);//遍历所有的节点

    return 0;
  }

  PNODE create_list(void)
  {
      int len;//用来存放有效节点的个数
      int val;//用来临时存放用户输入的节点的值
      int i;
      PNODE pHead=(PNODE)malloc(sizeof(NODE));
      PNODE pTail=pHead;
      pTail->pNext=NULL;//当用户只输入0个节点时
      if(pHead==NULL)
      {
        printf("分配内存失败，程序终止！");
        exit(-1);
      }
      printf("请用户输入节点的长度len= ");
      scanf("%d",&len);
      for(i=0;i<len;i++)
    {
        printf("请用户输入节点的值",i+1);
        scanf("%d",&val);
        PNODE pNew=(PNODE)malloc(sizeof(NODE));
         if(pNew==NULL)
      {
        printf("分配内存失败，程序终止！");
        exit(-1);
      }
       pNew->data=val;
       pTail->pNext=pNew;
       pNew->pNext=NULL;
       pTail=pNew;
    }

  return (pHead);

  }

  void traverse_list(PNODE pHead)
  {
      PNODE p=pHead->pNext;
      while(p!=NULL)
      {
       printf("%d ",p->data);
       p=p->pNext;
      }
      printf("\n");
      return;
  }


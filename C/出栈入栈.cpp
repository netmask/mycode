#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
typedef struct Node
{
  int data;
  struct Node *pNext;
}NODE,*PNODE;

//栈可以想象为一个箱子，放东西（进栈）是从栈底放起往上添加，拿出来（出栈）是从上面先来。
//所以执行先进后出
typedef struct Stack       //虽然产生了栈顶，和栈底，但都是垃圾数字，不是真正意义上的
{
    PNODE pTop;//栈顶
    PNODE pBottom;//栈底
}STACK,*PSTACK;//PSTACK等价于struct Stack*

void init(PSTACK);//进行初始化
void push(PSTACK ,int );//入栈
void traverse(PSTACK );//遍历输出
bool empty(PSTACK );//判断是否为空
bool pop(PSTACK ,int* );//出栈
void clear(PSTACK );//进行清空

int main(void)
{
  STACK S;//等价于struct Stack*S
  int val;//定义保存出栈时的值
  init(&S);
 push(&S,1);//压栈
  push(&S,2);
  push(&S,3);
  push(&S,4);
  push(&S,5);
  traverse(&S);//遍历输出
 clear(&S);
if(pop(&S,&val))
{
    printf("出栈成功！出栈的元素是：\n%d ",val);
}
else
{
    printf("出栈失败！");
}
traverse(&S);
  return 0;
}
void init(PSTACK pS)
{
   pS->pTop=(PNODE)malloc(sizeof(NODE));//为栈顶申请动态内存
   if(pS->pTop==NULL)
   {
       printf("分配内存失败!\n");
       exit(-1);
   }
   else
   {
       pS->pBottom = pS->pTop;  //在还没存入数据时，栈顶，栈顶都指向相同的地址
       pS->pTop->pNext=NULL;
   }
}

void push(PSTACK pS,int val)
{
    PNODE pNew=(PNODE)malloc(sizeof(NODE));
    pNew->data=val;
    pNew->pNext=pS->pTop;   //压栈时让栈顶指向新生成的节点
    pS->pTop=pNew;
    return;
}

void traverse(PSTACK pS)
{
    PNODE p=pS->pTop;
    while(p!=pS->pBottom)//从栈顶一直查到栈底并不断输出
    {
        printf("%d ",p->data);
        p=p->pNext;
    }
    printf("\n");
    return;
}
bool empty(PSTACK pS)
{
    if(pS->pTop==pS->pBottom)//判断栈是否为空

      return true;

    else

        return false;

}
bool pop(PSTACK pS,int *pVal)
{
    if(empty(pS))//由于引进来时pS就是地址
    {
        return false;
    }
    else
    {
        PNODE r=pS->pTop;
        *pVal=r->data;//将出栈的值传给*pVal;
        pS->pTop=r->pNext;
        free(r);
        r=NULL;
        return true;
    }
}
//清除
void clear(PSTACK pS)
{
    if(empty(pS))
    {
        return;
    }
    else
    {
        PNODE p=pS->pTop;//让p在q之上，往前移动然后删除p这样就实现了从栈顶一直往下删
        PNODE q=NULL;
        while(p!=pS->pBottom)
        {
            q=p->pNext;
            free(p);
            p=q;
        }
        pS->pTop=pS->pBottom;
    }
}


#include<stdio.h>
#include<malloc.h>
struct BTNode{
char data;
struct BTNode * pLchild;
struct BTNode * pRchild;
};
void PreTraverseBTree(struct BTNode * pT);
void INTraverseBTree(struct BTNode * pT);
void PoseTraverseBTree(struct BTNode * pT);
struct BTNode *CreateBTree(void);

int main(void)
{
struct BTNode * pT=CreateBTree();
printf("先序遍历\n");
PreTraverseBTree(pT);
printf("中序遍历\n");
INTraverseBTree(pT);
printf("后序遍历\n");
PoseTraverseBTree(pT);
return 0;
}
//********先序遍历********//
void PreTraverseBTree(struct BTNode * pT)
{
 if(NULL!=pT)//当它不为空时才进行输出，不然会报错
 {
     printf("%c\n",pT->data);
 }
 if(NULL!=pT->pLchild)//首先判断不为空，节省时间
 {
    PreTraverseBTree(pT->pLchild);
 }
 if(NULL!=pT->pRchild)
 {
    PreTraverseBTree(pT->pRchild);
 }
}
//********中序遍历********//
void INTraverseBTree(struct BTNode * pT)
{
 if(NULL!=pT)//当它不为空时才进行输出，不然会报错
 {


    if(NULL!=pT->pLchild)//首先判断不为空，节省时间
   {
      INTraverseBTree(pT->pLchild);
   }

    printf("%c\n",pT->data);

   if(NULL!=pT->pRchild)
   {
      INTraverseBTree(pT->pRchild);
   }
 }
}
//********后序遍历********//
void PoseTraverseBTree(struct BTNode * pT)
{
 if(NULL!=pT)//当它不为空时才进行输出，不然会报错
   {


   if(NULL!=pT->pLchild)//首先判断不为空，节省时间
    {
      PoseTraverseBTree(pT->pLchild);
    }
    if(NULL!=pT->pRchild)
     {
       PoseTraverseBTree(pT->pRchild);
     }
    printf("%c\n",pT->data);
   }
}

 struct BTNode *CreateBTree(void)//创建二叉树，简单静态的
 {
     struct BTNode * pA=(struct BTNode *)malloc(sizeof(struct BTNode));
     struct BTNode * pB=(struct BTNode *)malloc(sizeof(struct BTNode));
     struct BTNode * pC=(struct BTNode *)malloc(sizeof(struct BTNode));
     struct BTNode * pD=(struct BTNode *)malloc(sizeof(struct BTNode));
     struct BTNode * pE=(struct BTNode *)malloc(sizeof(struct BTNode));
     pA->data='A';
     pB->data='B';
     pC->data='C';
     pD->data='D';
     pE->data='E';

     pA->pLchild=pB;
     pA->pRchild=pC;
     pB->pLchild=pB->pRchild=NULL;
     pC->pLchild=pD;
     pC->pRchild=NULL;
     pD->pLchild=NULL;
     pD->pRchild=pE;
     pE->pLchild=pE->pRchild=NULL;
     return pA;//返回根节点


 }

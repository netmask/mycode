/* 线性结构的俩种常见应用之二——队列
 定义：
   一种实现“先进先出”的存储结构
分类：
链式队列

静态队列
静态队列通常都必须是循环队列                          判断队列是否已满：
队列需要俩个参数即front 头和rear尾                         if((rear+1)%数组长度)==f)
入队：rear=(rear+1)%数组长度                                    已满
出队：front=(front+1)%数组长度                   		    else
                                                            未满
*/
#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
//队列一定是循环的
typedef struct Queue{
    int *pBase;
    int front;
    int rear;
}QUEUE;

void init(QUEUE *);//队列的初始化
bool en_queue(QUEUE *,int val);//入队pQ->rear=(pQ->rear+1)%6;
void traverse_queue(QUEUE*);//遍历输出
bool full_queue(QUEUE*);//判断是否为满pQ->rear+1)%6==pQ->front
bool out_queue(QUEUE *,int*);//出队 pQ->front=(pQ->front+1)%6;
bool emput_queue(QUEUE *);//判断是否为空pQ->front==pQ->rear

int main(void)
{
    int val,val1;
    QUEUE Q;
    init(&Q);  //通过取地址将实参传入形参
    printf("入队的元素以0结束\n");
    while(val1!=0)                       //通过while循环入队
    {
      printf("请输入入队的元素：");
      scanf("%d",&val1);
      en_queue(&Q,val1);
    }

    traverse_queue(&Q);
    printf("\n");
    while(out_queue(&Q,&val))     //用while进行出队
    {
      printf("出队的元素是:%d\n",val);
    }
    /*if(out_queue(&Q,&val))
    {
       printf("出队成功！出队的元素是 %d",val);
    }
    else{
    printf("出队失败！");
    }*/
    return 0;

}
void init(QUEUE *pQ)    //队列的初始化
{
   pQ->pBase=(int*)malloc(sizeof(int)*6);//申请队列的长度为6由于队列是循环队列所以只能
   //存5个值，即始终是头尾只有一个存值
   pQ->front=0;
   pQ->rear=0;
}

bool full_queue(QUEUE *pQ)
{
    if((pQ->rear+1)%6==pQ->front)        //判断队满的标志
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool en_queue(QUEUE *pQ,int val)
{
    if(full_queue(pQ))
    {
        return false;
    }
    else{
     pQ->pBase[pQ->rear]=val;//入队，将val值添加在尾的后面
     pQ->rear=(pQ->rear+1)%6;
     return true;
    }
}
void traverse_queue(QUEUE *pQ)
{
    int i=pQ->front;      //从头开始
    while(i!=pQ->rear)  //直到尾
    {
        printf("%d ",pQ->pBase[i]);
        i=(i+1)%6;   //实际上等同于i++
    }
    return;
}
bool emput_queue(QUEUE *pQ)
{
    if(pQ->front==pQ->rear)    //为空的标志
    return true;
    else
    return false;
}
bool out_queue(QUEUE *pQ,int *pVal)//出队，将出队的值保存在PVal中
{
    if(emput_queue(pQ))
    {
       return false;
    }
    else
    {
      *pVal=pQ->pBase[pQ->front];//出队，从头开始出队将出队的值保存在PVal中
      pQ->front=(pQ->front+1)%6;
    }
}

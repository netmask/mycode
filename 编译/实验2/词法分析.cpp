#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
using namespace std;

const char strch[35][20] = {"int","float","double","return","char",
                           "while","if","else","break","continue",
                           "const","struct","for","do","auto",
                           "short","long","union","enum","tydef",
                           "unsigned","signed","extern","static",
                           "void","switch","case","goto","default",
                           "sizeof","bool"};

int getdata2 (char c)
{
    if (c >= '0' && c <= '9')
        return 0;
    if (c == '.')
        return 1;
    if (c == '+')
        return 2;
    if (c == '-')
        return 3;
    if (c == 'e')
        return 4;
}

bool check2 (char str[])
{
    int a[10][10];
    a[0][0] = 1;a[0][1] = -1;a[0][2] = -1;a[0][3] = -1;a[0][4] = -1;
    a[1][0] = 1;a[1][1] =  2;a[1][2] = -1;a[1][3] = -1;a[1][4] =  4;
    a[2][0] = 3;a[2][1] = -1;a[2][2] = -1;a[2][3] = -1;a[2][4] = -1;
    a[3][0] = 3;a[3][1] = -1;a[3][2] = -1;a[3][3] = -1;a[3][4] =  4;
    a[4][0] = 6;a[4][1] = -1;a[4][2] =  5;a[4][3] =  5;a[4][4] = -1;
    a[5][0] = 6;a[5][1] = -1;a[5][2] = -1;a[5][3] = -1;a[5][4] = -1;
    a[6][0] = 6;a[6][1] = -1;a[6][2] = -1;a[6][3] = -1;a[6][4] = -1;
    int t = 0,i = 0;
    while (str[i])
    {
        int k = getdata2 (str[i]);
        t = a[t][k];
        if (t == -1)
            return false;
        i++;
    }
    if (t != 1 && t != 3 && t != 6)
        return false;
    return true;
}

int getdata1 (char c)
{
    if (c == '_' || (c >= 'a' && c<= 'z') || (c >= 'A' && c <= 'Z'))
        return 0;
    if (c >= '0' && c <= '9')
        return 1;
    return -1;
}

bool check1(char str[])   //标识符
{
    int a[5][5];
    a[0][0] = 1;  a[0][1] = -1;
    a[1][0] = 1;  a[1][1] =  1;
    int t = 0,i = 0;
    while (str[i])
    {
        int k = getdata1 (str[i]);
        if (k == -1)
            return false;
        t = a[t][k];
        if (t == -1)
            return false;
        i++;
    }
    if (t != 1)
        return false;
    return true;
}

void check (char str[],int flag)
{
    for (int i = 0;i < 35;i++)
        if (!strcmp (str,strch[i]))
        {
            printf ("(1,%s)\n",str);
            return ;
        }
    if (flag == 4)
    {
        printf ("(4,%s)\n",str);
        return ;
    }
    if (str[0] >= '0' && str[0] <= '9' || str[0] == '.')
    {
        int i = 0;
        while (str[i])
        {
            if (!(str[i] >= '0' && str[i] <= '9') && str[i] != 'e' && str[i] != '.'
                && str[i] != '-' && str[i] != '+')
            {
                if (check1(str))
                {
                    printf ("(2,%s)\n",str);
                    return ;
                }
                else
                {
                    printf ("(2,%s,error)\n",str);
                    return ;
                }
            }
            i++;
        }
        if (check2(str))
        {
            printf ("(3,%s)\n",str);
            return ;
        }
        if (!check2(str))
        {
            printf ("(3,%s,error)\n",str);
            return ;
        }
    }
    if (check1(str))
    {
        printf ("(2,%s)\n",str);
        return ;
    }
    if (!check1(str))
    {
        printf ("(2,%s,error)\n",str);
    }
}

void input (char str[])     //接收字符串str
{
    char str0[100] = "\0";           //定义一个空的字符串str0
        int t = 0,flag = -1,flag1 = 0;int i;   //flag表示类型，flag1表示是否为科学记数法
        for (i = 0;str[i];i++)
        {
            if (str[i] == ' ')  //判断str是否为空
            {
                if (str0[0])
                {
                    str0[t++] = '\0';
                    check (str0,flag);
                    flag = -1;
                    str0[0] = '\0';
                    t = 0;
                }
                str0[0] = '\0';
                continue;
            }
            if (((str[i] == '+' && str[i-1] == 'e')   //判断是否为科学
				|| (str[i] == '-' && str[i-1] == 'e')) && flag == 3 && flag1)
            {
                str0[t++] = str[i];
                flag1 = 0;
                continue;
            }
            if (str[i] == ',' || str[i] == ';' || str[i] == '\"' ||
                str[i] == '(' || str[i] == ')' || str[i] == '['  ||
                str[i] == ']' || str[i] == '{' || str[i] == '}')     //判断界符
            {
                if (str0[0])
                {
                    str0[t++] = '\0';
                    check (str0,flag);
                    flag = -1;
                    str0[0] = '\0';
                    t = 0;
                }
                str0[0] = '\0';
                printf ("(5,%c)\n",str[i]);
                continue;
            }
            if (str[i] == '=' || str[i] == '+' || str[i] == '-' ||
                str[i] == '*' || str[i] == '/' || str[i] == '>' ||
                str[i] == '<')                                        //运算符
            {
                if (str0[0] && flag != 4)
                {
                    str0[t++] = '\0';
                    check (str0,flag);
                    flag = -1;
                    str0[0] = '\0';
                    t = 0;
                }
                str0[t++] = str[i];
                flag = 4;
                continue;
            }
            if (str[i] == 'e' && flag == 3) //判断是否为科学计数法
            {
                flag1 = 1;
                str0[t++] = 'e';
                continue;
            }
            if (str[i] >= '0' && str[i] <= '9' ||
                str[i] == '.')                               //实数
            {
                if (flag == 0)
                {
                    str0[t++] = str[i];
                    continue;
                }
                if (str0[0] && flag != 3)
                {
                    str0[t++] = '\0';
                    check (str0,flag);
                    flag = -1;
                    str0[0] = '\0';
                    t = 0;
                }
                str0[t++] = str[i];
                flag = 3;
                continue;
            }
            if (str0[0] == '=' || str0[0] == '+' || str0[0] == '-' ||
                str0[0] == '*' || str0[0] == '/' || str0[0] == '>' ||
                str0[0] == '<')
            {
                str0[t++] = '\0';
                printf ("(4,%s)\n",str0);
                t = 0;
                flag = -1;
            }
            flag = 0;
            str0[t++] = str[i];
        }
		if (!str[i] && str0[0])
		{
			str0[t]='\0';
            check (str0,flag);
            flag = -1;
            str0[0] = '\0';
		}
}

int main ()
{
    char str[10000];
    freopen ("in.txt","r",stdin);
    freopen ("out.txt","w",stdout);
    while (gets (str))
    {
        input (str);
    }
    return 0;
}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

int get_r (char c)
{
    if (c == 'i')
        return 0;
    if (c == '+' || c == '-')
        return 1;
    if (c == '*' || c == '/')
        return 2;
    if (c == '(')
        return 3;
    if (c == ')')
        return 4;
    if (c == '#')
        return 5;
    return -1;
}

int get_c (char c)
{
    if (c == 'E')
        return 0;
    if (c == 'M')
        return 1;
    if (c == 'T')
        return 2;
    if (c == 'N')
        return 3;
    if (c == 'F')
        return 4;
    return -1;
}

bool check3 (char a,char b)
{
    if (a == b)
        return true;
    if (a == '+' && b == '-')
        return true;
    if (a == '*' && b == '/')
        return true;
    return false;
}

bool LL_1 (char str[])
{
    bool flag = true;
    char st[10][5] ={"","MT","MT+","NF","NF*","i",")E("};
    int a[8][8];
    a[0][0] =  1; a[0][1] = -1; a[0][2] = -1; a[0][3] =  1; a[0][4] = -1; a[0][5] = -1;
    a[1][0] = -1; a[1][1] =  2; a[1][2] = -1; a[1][3] = -1; a[1][4] =  0; a[1][5] =  0;
    a[2][0] =  3; a[2][1] = -1; a[2][2] = -1; a[2][3] =  3; a[2][4] = -1; a[2][5] = -1;
    a[3][0] = -1; a[3][1] =  0; a[3][2] =  4; a[3][3] = -1; a[3][4] =  0; a[3][5] =  0;
    a[4][0] =  5; a[4][1] = -1; a[4][2] = -1; a[4][3] =  6; a[4][4] = -1; a[4][5] = -1;

    char stack[1000] = "#E";
    int top =  1,k = 0;
    while (str[k])
    {
        for (int i = 0;i <= top;i++)
            cout<<stack[i];
        cout<<"   ";
        for (int i = k;str[i];i++)
            cout<<str[i];
        cout<<endl;
        char c_c = stack[top];
        top--;
        if (c_c == '#')
        {
            if (str[k] == '#')
            {
                flag = true;
                break;
            }
            else
            {
                flag = false;
                break;
            }
        }
        int c = get_c (c_c);
        if (c == -1)
        {
            if (check3 (c_c,str[k]))
            {
                k++;
                continue;
            }
            else
            {
                flag = false;
                break;
            }
        }
        else
        {
            int r = get_r (str[k]);
            if (r == -1)
            {
                flag = false;
                break;
            }
            int t = a[c][r];
            if (t == -1)
            {
                flag = false;
                break;
            }
            for (int i = 0;st[t][i];i++)
            {
                top++;
                stack[top] = st[t][i];
            }
        }
    }
    return flag;
}

int main ()
{
    freopen ("in.txt","r",stdin);
    freopen ("out.txt","w",stdout);
    char str[1010];
    while (scanf ("%s",str) != EOF)
    {
        if (LL_1 (str))
            printf ("YES\n");
        else
            printf ("NO\n");
    }
    return 0;
}

#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#define N 4
#define epsilon 1e-6

int main()
{
	double arr[N][N+1] =
	{
		10,       -7, 0,  1, 8,
		-3, 2.099999, 6,  2, 5.900001,
		 5,       -1, 5, -1, 5,
		 2,        1, 0,  2, 1
	};

	int i, j, k;
	double sum;
	//交换两列
	for (k=0; k<N; k++)
	{
		for (i=k+1; i<N; i++)//选主元
			if (abs((int)arr[i][k]) > abs((int)arr[k][k]))
				for (j=k; j<N+1; j++)
				{
					sum = arr[k][j];
					arr[k][j] = arr[i][j];
					arr[i][j] = sum;
				}

		if (abs((int)arr[k][k]) < epsilon)
		{
			printf("无法消元\n");
			return 0;
		}

		for (i=k+1; i<N; i++)//消元
		{
			arr[i][k] /= arr[k][k];
			for (j=k+1; j<N+1; j++)
				arr[i][j] -= arr[i][k] * arr[k][j];
		}

		printf("第%d次消元:\n", k+1);
		for(i=0; i<N; i++)
		{
			for(j=0; j<N+1; j++)
				printf("%0.4lf ", arr[i][j]);
			printf("\n");
		}
	}

	arr[N-1][N] /= arr[N-1][N-1];
	for (k=N-2; k>=0; k--)//回代
	{
		sum = 0;
		for (j=k+1; j<N; j++)
			sum += arr[k][j] * arr[j][N];
		arr[k][N] = (arr[k][N] - sum) / arr[k][k];
	}

	printf("\n结果为:\n");
	for (i=0; i<N; i++)
		printf("X[%d]= %.4f\n", i, arr[i][N]);
		return 0;
}

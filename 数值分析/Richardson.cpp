#include <stdio.h>
#include <math.h>
#define N 50
#define X 1
#define e 2.718281828459

double f(double x)
{
	return pow(e, x);
}

double cal(int k, double h)
{
	double x = h / pow(2, k);
	return pow(2, k-1) * (f(X + x) - f(X - x)) / h;
}

int main()
{
	double n, h;
	double G[N][N] = {0};
	h = 1 ;
	int j, k = 0;

    G[0][0] = cal(k, h);

	do
	{
		k += 1;
		G[k][0] = cal(k, h);
		j = 1;
		while (j <= k)
		{
			n = pow(4.0, j);
			G[k][j]= (n * G[k][j-1] - G[k-1][j-1]) / (n - 1);
			j++;
		}
	} while(fabs(G[k][k] - G[k-1][k-1]) > pow(10, -8));

	printf("f`(1)=%0.12lf",G[k][k]);














   /* for(int i=0; i<k+1; i++)
	{
		for(j=0; j<=i; j++)
			printf("%0.12lf  ", G[i][j]);
		printf("\n");
	}*/
}

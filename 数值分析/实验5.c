#include <stdio.h>
#include <math.h>
//#define e 1e-10
#define a 0                          //积分下限a
#define b 3.14159265358979323846                          //积分上限b
#define f(x) (pow(M_E, x) * cos(x))           //被积函数f(x)
int main()
{
    int i,n;
    double h,t0,t,g;
    n=1;                             //赋初值
    h=(double)(b-a);
    t=h*(f(a)+f(b))/2;
    do
    {
       t0=t;
       g=0;
       for (i=1;i<=n;i++)
           g+=f((a+(2*i-1)*h/2));
       t=(t0/2)+(h*g)/2;                //复化梯形公式
       n*=2;
       h/=2;
    }
    while (fabs(t-t0)>10e-6);             //自定义误差限e
    printf("%.10lf %d",t,i/2);                //输出积分的近似值

return 0;
}

data	segment
	mess	db	'Student Information Management System',0dh,0ah
		db	"[1]show the student's informations",0dh,0ah
		db	'[2]add one',0ah,0dh
		db	'[3]amend one',0ah,0dh
		db	'[4]delete one',0ah,0dh,'$'
	;####################################################
	array_byte	db	2 dup(?)
	array_word	dw	2 dup(?)
	address		dd	mess
data	ends
code	segment
assume cs:code,ds:data
start:	
	mov	ax,data
	mov	ds,ax
	lea	dx,mess
	mov	ah,9
	int	21h
	;####################################################
	;enter your code here
	lea bx,array_byte
	mov byte ptr [bx],1
	inc bx
	mov byte ptr [bx],2

	lea bx,array_byte
	mov al,[bx]
	xchg al,[bx+1]
	mov [bx],al

	mov bx,0
	mov array_word[bx],3
	inc bx
	inc bx
	mov array_word[bx],4

	push array_word[bx]
	push array_word[bx-2]
	pop array_word[bx]
	pop array_word[bx-2]

	les si,address
	mov dl,es:[si]
	;####################################################
	;the last two instructions' function is program exit.
	mov	ah,4ch
	int	21h
	code	ends
end	start

DATAS SEGMENT
   W   dw     2
   X   dw     4
   Y   dw     1
   Z   dw	   ?
   R   dw      ?    
DATAS ENDS
CODES SEGMENT
    ASSUME CS:CODES,DS:DATAS
START:
    MOV AX,DATAS
    MOV DS,AX
    ;此处输入代码段代码
    ;###############################################
     MOV     AX,W
     IMUL    X
     MOV     CX,AX
     MOV     AX,6
     ADD     AX,Y
     MOV     BX,AX
     MOV     AX,CX
     DIV     BX
     MOV     Z,AX
     MOV     R,DX
     mov     CX,DX
     
     MOV     BL,al 
     call b2d
     
     
     MOV       DL,0DH 
     MOV       AH,2
     INT       21H
              
     MOV       DL,0AH
     MOV       AH,2
     INT       21H         
	
      MOV     BL,Cl 
      call b2d
     
;######################################     
    mov	ah,4ch
	int	21h
;######################################
;subfunction name : b2d
;function : output a positive integer number ([0 , 65535])
;usage: mov ax,number
	;      call d2b
;input parameter : ax
;output parameter : null
b2d	proc	near
	PUSH AX
	PUSH CX
	PUSH DX
	MOV AX,-1
	PUSH AX
	MOV CX,10
LOP2:
	MOV AX,BX
	MOV DX,0
	DIV CX
	PUSH DX
	CMP AX,0
	JZ SHOW
	MOV BX,AX
	JMP LOP2
SHOW:
	POP DX
	CMP DX,-1
	JZ EXIT2
	ADD DL,30H
	MOV AH,2
	INT 21H
	JMP SHOW
EXIT2:
	POP DX
	POP CX
	POP AX
	RET
b2d	endp
     
CODES ENDS
    END START

        DATA  SEGMENT
;enter your variable definition here

        ID_1  DB        'show the number of student from hight to low ' ,0DH,0AH
              DB        'show the grade of student from hight to low'   ,0DH,0AH,'$'

        ID_2  DB        'input the number of student ' ,0DH,0AH,'$'
        ID_3  DB        'input the name of student ' ,0DH,0AH,'$'
        ID_4  DB        'input the age of student ' ,0DH,0AH,'$'
        ID_5  DB        'input the sex of student ' ,0DH,0AH,'$'




;######################################
     STUDENT_INFORMATION  LABEL     BYTE
  STUDENT_ID  DB        10,?,10 DUP('$')
            STUDENT_NAME  DB        22,?,22 DUP('$')
          STUDENT_GENDER  DB        ?
           STUDENT_GRADE  DB        ?

        DATA  ENDS
        CODE  SEGMENT
              ASSUME    CS:CODE,DS:DATA
      START:
              MOV       AX,DATA
              MOV       DS,AX
;enter your code here

              LEA       DX,ID_1
              MOV       AH,9
              INT       21H

              LEA       DX,ID_2
              MOV       AH,9
              INT       21H
              
              CALL      D2B
              

              LEA       DX,ID_3
              MOV       AH,9
              INT       21H



              LEA       DX, STUDENT_ID
              MOV       AH,10
              INT       21H
              
             
             
              
              MOV       DL,0DH
              MOV       AH,2
              INT       21H
              
              MOV       DL,0AH
              MOV       AH,2
              INT       21H
              
             

              LEA       DX,ID_4
              MOV       AH,9
              INT       21H
              
              MOV       DL,0DH
              MOV       AH,2
              INT       21H
              
              MOV       DL,0AH
              MOV       AH,2
              INT       21H
              
          

              CALL      D2B
              
              

              LEA       DX,ID_5
              MOV       AH,9
              INT       21H
              
              MOV       DL,0DH
              MOV       AH,2
              INT       21H
              
              MOV       DL,0AH
              MOV       AH,2
              INT       21H


              LEA       DX,STUDENT_NAME
              MOV       AH,10
              INT       21H

              MOV       DL,0DH
              MOV       AH,2
              INT       21H

              MOV       DL,0AH
              MOV       AH,2
              INT       21H

              LEA       DX, STUDENT_ID
              MOV       AH,9
              INT       21H
              
              MOV       DL,0DH
              MOV       AH,2
              INT       21H
              
              MOV       DL,0AH
              MOV       AH,2
              INT       21H
             

              LEA       DX,STUDENT_NAME+2
              MOV       AH,9
              INT       21H

              MOV       DL,0DH
              MOV       AH,2
              INT       21H
              
              MOV       DL,0AH
              MOV       AH,2
              INT       21H
             
              CALL      B2D
             

;##################################
              MOV       AH,4CH
              INT       21H
;######################################
;subfunction name : d2b
;function : input a positive integer number ([0 , 65535])
;usage: call d2b
;input parameter : null
;output parameter : bx
         D2B  PROC      NEAR
              PUSH      AX
              PUSH      CX
              MOV       BX,0
              MOV       CX,10
       LOP1:
              MOV       AH,1
              INT       21H
              CMP       AL,0DH
              JZ        EXIT1
              SUB       AL,30H
              MOV       AH,0
              XCHG      AX,BX
              MUL       CX
              ADD       BX,AX
              JMP       LOP1
      EXIT1:
              POP       CX
              POP       AX
              RET
         D2B  ENDP
;######################################
;subfunction name : b2d
;function : output a positive integer number ([0 , 65535])
;usage: mov ax,number
	;      call d2b
;input parameter : ax
;output parameter : null
         B2D  PROC      NEAR
              PUSH      AX
              PUSH      CX
              PUSH      DX
              MOV       AX,-1
              PUSH      AX
              MOV       CX,10
       LOP2:
              MOV       AX,BX
              MOV       DX,0
              DIV       CX
              PUSH      DX
              CMP       AX,0
              JZ        SHOW
              MOV       BX,AX
              JMP       LOP2
       SHOW:
              POP       DX
              CMP       DX,-1
              JZ        EXIT2
              ADD       DL,30H
              MOV       AH,2
              INT       21H
              JMP       SHOW
      EXIT2:
              POP       DX
              POP       CX
              POP       AX
              RET
         B2D  ENDP
        CODE  ENDS
              END       START





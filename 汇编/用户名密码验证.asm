;实验四
;编写程序实现用户名、密码的输入和验证及修改程序。
;选作功能：用户名和密码加密后保存。
;说明：由于没有文件操作，预置的用户名、密码事先保存在变量中即可。当第一次登陆成功以后，用户可以修改用户名和密码。

data	segment
;#######变量定义############
id		label	byte		;等价于id	db	3,'abc',17 dup(?)
id_len		db	3
id_content	db	'abc',17 dup(?),'$'
pw		label	byte		;等价于pw	db	3,'123',17 dup(?)
pw_len		db	3
pw_content	db	'123',17 dup(?),'$'
id_in		label	byte		;等价于id_in	db	21,?,21 dup(?)
id_in_max_len	db	21
id_in_real_len	db	?
id_in_content	db	21 dup(?)
pw_in		label	byte		;等价于pw_in	db	21,?,21 dup(?)
pw_in_max_len	db	21
pw_in_real_len	db	?
pw_in_content	db	21 dup(?)
login		db	0		;0表示未登陆成功，1表示登陆成功
;#######提示语定义############
mess_welcome	db	'Welcome to my ID&PW System!$'
mess_input_id	db	'ID:$'
mess_input_pw	db	'PW:$'
mess_login_fail	db	'ID or PW is wrong!$'
mess_login_succeed	db	'Login successfully!$'
mess_whether_mend	db	'Do you want to modify your id and pw?(y/n)$'
mess_mended	db	'Modify id and pw successfully!$'
lr	db	0dh,0ah,'$'
data	ends
code	segment
	assume	cs:code,ds:data,es:data
start:
	mov	ax,data
	mov	ds,ax
	mov	es,ax
	;输出欢迎词
welcome:
	lea	dx,mess_welcome
	mov	ah,9
	int	21h
	jmp	short go_on
wrong_id_pw:
	;输出提示信息
	lea	dx,mess_login_fail
	mov	ah,9
	int	21h
go_on:	
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	;输出提示信息
	lea	dx,mess_input_id
	mov	ah,9
	int	21h
	;输入用户名
	lea	dx,id_in
	mov	ah,10
	int	21h
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	;输出提示信息
	lea	dx,mess_input_pw
	mov	ah,9
	int	21h
	;输入密码
	lea	dx,pw_in
	mov	ah,10
	int	21h
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	;检查用户名长度是否相等
	mov	cl,id_in_real_len
	cmp	cl,id_len
	jnz	wrong_id_pw
	;检查用户名内容是否相同
	mov	ch,0  ;将高8位变0
	lea	si,id_in_content
	lea	di,id_content
	cld
	repe	cmpsb
	jnz	wrong_id_pw
	;检查密码长度是否相等
	mov	cl,pw_in_real_len
	cmp	cl,pw_len
	jnz	wrong_id_pw
	;检查密码内容是否相同
	mov	ch,0
	lea	si,pw_in_content
	lea	di,pw_content
	cld
	repe	cmpsb
	jnz	wrong_id_pw
	;成功登陆
	mov	login,1
	;检查登陆状态
	cmp	login,1
	jz	continue1
	jmp	welcome
continue1:
	;输出提示信息
	lea	dx,mess_login_succeed
	mov	ah,9
	int	21h
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	;输出提示信息
	lea	dx,mess_whether_mend
	mov	ah,9
	int	21h
	;输入y或非y的字符
	mov	ah,1
	int	21h
	;如果是y则修改用户名密码
	cmp	al,'y'
	jz	continue2
	jmp	exit
continue2:
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	;输出提示信息
	lea	dx,mess_input_id
	mov	ah,9
	int	21h
	;输入用户名
	lea	dx,id_in
	mov	ah,10
	int	21h
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	;输出提示信息
	lea	dx,mess_input_pw
	mov	ah,9
	int	21h
	;输入密码
	lea	dx,pw_in
	mov	ah,10
	int	21h
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	;更新用户名长度
	mov	cl,id_in_real_len
	mov	id_len,cl
	;更新用户名
	mov	ch,0
	lea	si,id_in_content
	lea	di,id_content
	cld
	rep	movsb
	;更新密码长度
	mov	cl,pw_in_real_len
	mov	pw_len,cl
	;更新密码内容
	mov	ch,0
	lea	si,pw_in_content
	lea	di,pw_content
	rep	movsb
	;输出提示信息
	lea	dx,mess_mended
	mov	ah,9
	int	21h
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	lea	dx,id_content
	mov	ah,9
	int	21h
	;输出换行回车
	lea	dx,lr
	mov	ah,9
	int	21h
	lea	dx,pw_content
	mov	ah,9
	int	21h
exit:
	mov	ah,4ch
	int	21h
code	ends
	end	start
﻿/**********************************************
*********代表权限项用到的一些函数、枚举变量****
* add by guofc on 2009.07.15*******************
**********************************************/
var Popedom=new Object();

//代表“PopedomType”枚举值
Popedom.TYPE_ORGANIZATION=0;
Popedom.TYPE_ROLE=1;
Popedom.TYPE_USER=2;

//IssueScope的枚举值
Popedom.ISSUESCOPE_SITE=0;
Popedom.ISSUESCOPE_CHANNEL=1;
Popedom.ISSUESCOPE_DOCUMENT=2;
Popedom.ISSUESCOPE_FRAGMENT=3;

//代表web服务返回的状态代码
Popedom.POPEDOMCODE_SUCCEED=0;

Popedom.PrefixOfHylink="C9660386-A5AD-4123-BB9E-283939B1E538";

//代表权限部署url地址
Popedom.BaseUrl=GetWccmServiceUrl();

Popedom.DocList=function(baseUrl,portletId,templateId,channelId,siteId,docId,invoker)
{
	this.DocListId=portletId+"_"+templateId+"_"+channelId+"_"+siteId;
	this.HolderId= portletId+"_"+templateId+"_"+channelId+"_"+siteId+"_div";	
	
    var InnerHtml="<div id='"+this.HolderId+"' style='vertical-align:bottom;width:100px;height:100px;text-align:center;display:block' ><img src='"+baseUrl+"Images/ajax-arrow.gif'/></div>";
	var oHolderDiv=document.getElementById(this.DocListId);
	if(oHolderDiv!=null){oHolderDiv.innerHTML=InnerHtml;}
	
	var onload=PortletHTML;
    var url=baseUrl+"AnalysisPopedom.asmx/OutputDocListHTML";
    var params="portletId="+portletId+"&templateId="+templateId+"&channelId="+channelId+"&siteId="+siteId+"&docId="+docId+"&invoker="+invoker;
	var ContentLoader=new net.ContentLoader(url,onload,params,this);
}

function PortletHTML()
{	
	 var responseText=arguments[0]; 
     if(responseText!=null)
     {
	 	var Succeed=ParseCodeForArray(responseText,0);
	 	var RetCode=ParseCodeForArray(responseText,1);	
		
		if(Succeed==Popedom.POPEDOMCODE_SUCCEED) //成功
		{			
			document.getElementById(this.context.DocListId).outerHTML=RetCode;	
		}
		else
		{
			document.getElementById(this.context.DocListId).outerHTML="";				
		}
     }
}
//得到wccm服务的url地址
function GetWccmServiceUrl()
{
	var fullUrl=window.location.href;
	var nIndex=fullUrl.lastIndexOf("/");
	return fullUrl.substring(0,nIndex+1);
}
//功能：得到XML文档模型
function getXmlDocument()
{
    var xDoc=null;
    if(document.implementation
        &&document.implementation.createDocument)
    {
       xDoc=document.implementation.createDocument("","",null);//Mozilla/Safari
    }
    else if(typeof ActiveXObject!="undefined")    
    {
        var msXmlAx=null;
        try
        {
            msXmlAx=new ActiveXObject("Msxml2.DomDocument");//较新版本IE
        }catch(e)
        {
            msXmlAx=new ActiveXObject("Msxml.DomDocument");//较旧版本IE
        }
        xDoc=msXmlAx;
    }
    if( xDoc==null||typeof xDoc.load== "undefined")
    {
        xDoc==null;
    } 
    return xDoc; 
}
//功能：根据返回的xml文件，返回第一个节点的文本。
function ParseOneResultCode(src)
{
	var xmlDoc = getXmlDocument();	
	xmlDoc.async = false;	
	xmlDoc.loadXML(src);
	if (xmlDoc.parseError.errorCode != 0) 
	{
		alert(xmlDoc.parseError);
		return xmlDoc.parseError.errorCode;
	}
	else
	{
		return xmlDoc.childNodes(1).text;
	}
}
//nIndex：代表数组第几个元素
function ParseCodeForArray(src,nIndex)
{
	var xmlDoc = getXmlDocument();
	xmlDoc.async = false;		
	xmlDoc.loadXML(src);
	if (xmlDoc.parseError.errorCode != 0) 
	{
		alert(xmlDoc.parseError);
		return xmlDoc.parseError.errorCode;
	}
	else
	{
		return xmlDoc.childNodes(1).childNodes(nIndex).text;
	}
}
﻿var net=new Object();

net.READY_STATE_UNINITIALIZED=0;
net.READY_STATE_LOADING=1;
net.READY_STATE_LOADED=2;
net.READY_STATE_INTERACTIVE=3;
net.READY_STATE_COMPLETE=4;

/*******判断浏览器类型*******************/
net.BROWSER_IE=100;
net.BROWSER_FIREFOX=110;
net.BROWSER_SAFARI=120;
net.BROWSER_CAMINO=130;
net.BROWSER_GECKO=140;
net.UNKNOWN=150;
net.getOS=function()
{
	var agent=navigator.userAgent;
	if(agent.indexOf("MSIE")>0) 
		return net.BROWSER_IE;
	else if(agent.indexOf("Safari")>0)
		return net.BROWSER_SAFARI;
	else if(agent.indexOf("Firefox")>0)
		return net.BROWSER_FIREFOX;
	else if(agent.indexOf("Camino")>0)
		return net.BROWSER_CAMINO;
	else if(agent.indexOf("Gecko")>0)
		return net.BROWSER_GECKO;
	else
		return net.UNKNOWN;

}
/***********结束浏览器类型判断**************/
net.ContentLoader=function(url,onload,params,context){
  this.req=null;
  this.onload=onload;
  this.onerror=this.defaultError;
  this.context=context;
  this.loadXMLDoc(url,params);
}

net.ContentLoader.prototype.loadXMLDoc=function(url,params){

  var method="POST";
  var contentType='application/x-www-form-urlencoded'; 
  var Browser=net.getOS();
  if(Browser!=net.UNKNOWN)
  {
	  if(Browser==net.BROWSER_IE)
	  {
		  this.req=new ActiveXObject("Microsoft.XMLHTTP");		  
		 
	  }
	  else
	  {
		  this.req=new XMLHttpRequest();
	  }	  
  }
  
  if (this.req)
  {

    try{
      var loader=this;
      this.req.onreadystatechange=function(){
        net.ContentLoader.onReadyState.call(loader);
      }      
      var absoluteUrl=Popedom.BaseUrl+url;
      if(url.toLowerCase().indexOf("http://")>-1) //绝对路径
      {
        absoluteUrl=url;
      }
      this.req.open(method,absoluteUrl,true);
      if (contentType){
        this.req.setRequestHeader('Content-Type', contentType);
      }

      this.req.send(params);
      
    }catch (err){
      this.onerror.call(this);
   }
 }
}
net.ContentLoader.onReadyState=function(){
    try
    {
      var req=this.req;
      var ready=req.readyState;
      
      if (ready==net.READY_STATE_COMPLETE)
      {
		  var httpStatus=req.status;
		  
        if (httpStatus==200 || httpStatus==0)
        {
            /*var Browser=net.getOS();
            if(Browser!=net.BROWSER_IE)
            {
                    var parser = new DOMParser();
    var xml = parser.parseFromString(req.responseText, "text/xml");
       alert(xml.childNodes[1].childNodes[0].firstChild.nodeValue);
            alert(xml.childNodes[0].firstChild.nodeValue); 
            }

            alert(req.responseText);  */
          this.onload.call(this,req.responseText);
        }else
        {
          this.onerror.call(this);
        }
      }
    }catch(e){}  
}

net.ContentLoader.prototype.defaultError=function(){
  alert("error fetching data!"
    +"\n\nreadyState:"+this.req.readyState
    +"\nstatus: "+this.req.status
    +"\nheaders: "+this.req.getAllResponseHeaders());/**/
}
package junit.test;


import org.junit.BeforeClass;
import org.junit.Test;

import cn.cmdoit.service.CGlibProxyFactroy;
import cn.cmdoit.service.JDKProxyFactroy;
import cn.cmdoit.service.PersonService;
import cn.cmdoit.service.impl.PersonServiceBean;

public class AOPTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	@Test public void proxyTest(){
		JDKProxyFactroy factroy=new JDKProxyFactroy();
		PersonService service=(PersonService)factroy.createProxyIntance(new PersonServiceBean("ss"));
		service.save("888");
	}
	
	@Test public void proxyTest1(){
		CGlibProxyFactroy factroy=new CGlibProxyFactroy();
		PersonServiceBean service=(PersonServiceBean)factroy.createProxyIntance(new PersonServiceBean("xx"));
		service.save("999");
	}
}

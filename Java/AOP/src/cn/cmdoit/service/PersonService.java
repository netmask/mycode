package cn.cmdoit.service;

public interface PersonService {
	public String getPersonname(int personid);
	public void save(String name);
	public void update(String name,int personid);
	
}

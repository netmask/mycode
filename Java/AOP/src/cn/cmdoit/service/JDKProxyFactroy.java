package cn.cmdoit.service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import cn.cmdoit.service.impl.PersonServiceBean;

public class JDKProxyFactroy implements InvocationHandler {
	private Object targetObject;
	
	public Object createProxyIntance(Object targetObject){
		this.targetObject=targetObject;
		return Proxy.newProxyInstance(this.targetObject.getClass().getClassLoader(),
				this.targetObject.getClass().getInterfaces(), this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		Object result=null;
		PersonServiceBean bean=(PersonServiceBean)this.targetObject;
		if(bean.getUser()!=null){
	    result=method.invoke(targetObject, args);
		}
		return result;
	}
	
}

package cn.cmdoit.service.impl;

import cn.cmdoit.service.PersonService;

public class PersonServiceBean {
	private String user;
	
	public String getUser() {
		return user;
	}

	public PersonServiceBean(){}
	
	public PersonServiceBean(String user){
		this.user=user;
	}
	
	public String getPersonname(int personid) {
		System.out.println("我是 getPersonname()方法");
		return null;
	}

	
	public void save(String name) {
		System.out.println("我是 save()方法");
	}

	
	public void update(String name, int personid) {
		System.out.println("我是 update()方法");
	}

}

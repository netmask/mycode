package com.cmdoit.user.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.cmdoit.user.Model.User;
import com.cmdoit.user.util.HibernateUitl;

public class UserDao implements IUserDao {

	public void add(User user) {
		// TODO Auto-generated method stub
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			s.save(user);
			tx.commit();
		}finally{
			if (s!=null)
			{
				s.close();
			}
		}
	}

	public User loadbyuser(String username) {
		// TODO Auto-generated method stub
		Session s=null;
		try{
			s=HibernateUitl.getSession();
			String hql="from User as user where user.username=:n";
			Query q=s.createQuery(hql);
			q.setString("n",username);
			User user=(User)q.uniqueResult();
			return user;
		}finally{
			if(s!=null)
				s.close();
		}
	}
	
}

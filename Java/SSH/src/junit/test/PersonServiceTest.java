package junit.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.spring.bean.Person;
import cn.spring.service.PersonService;



public class PersonServiceTest {
	@SuppressWarnings("unused")
	private static PersonService personService;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try {
			ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
			personService = (PersonService)applicationContext.getBean("personService");
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSave() {
		for(int i=0;i<5;i++)
		personService.save(new Person("xiaoliu"+i));
	}

	@Test
	public void testUpdate() {
		Person person=personService.getPerson(1);
		person.setName("xiaohong");
		personService.update(person);
	}

	@Test
	public void testGetPerson() {
		Person person=personService.getPerson(1);
		System.out.println(person.getName());
		try {
			System.out.println("请关闭数据库！");
			Thread.sleep(1000*15);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("第二次获取数据！");
		person=personService.getPerson(1);
		System.out.println(person.getName());
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPersons() {
		List<Person> persons=personService.getPersons();
		for(Person person:persons)
			System.out.println(person.getName());
	}

}

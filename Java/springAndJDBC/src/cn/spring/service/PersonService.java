package cn.spring.service;

import java.util.List;

import cn.spring.bean.Person;

public interface PersonService {
	/**
	 * 保存perosn记录
	 * @param person
	 */
	public void save(Person person);
	/**
	 * 更新person记录
	 * @param person
	 */
	public void update(Person person);
	/**
	 * 获取person记录
	 * @param personId
	 * @return
	 */
	public Person getPerson(int personId);
	/**
	 * 获取所有的person记录
	 * @return
	 */
	public List<Person> getPerson();
	/**
	 * 删除指定的id的person记录
	 * @param personId
	 */
	public void delete(int personId);
}

package cn.spring.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import cn.spring.bean.Person;

public class PersonRowMapper implements RowMapper {

	@Override
	public Object mapRow(ResultSet rs, int index) throws SQLException {
		Person person=new Person(rs.getString("name"));
		person.setId(rs.getInt("id"));
		return person;
	}

}

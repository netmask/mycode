package junit.test;


import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.spring.bean.Person;
import cn.spring.service.PersonService;

public class PersonServiceTest {
	private static PersonService personService;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try {
			ApplicationContext cxt = new ClassPathXmlApplicationContext("beans.xml");
			personService =(PersonService) cxt.getBean("personService");
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test public void save(){
		for(int i=0;i<5;i++)
		personService.save(new Person("��ѧSpring"+i));
	}
	
	@Test public void getPerson(){
		Person person=personService.getPerson(1);
		System.out.println(person.getName());
	}
	@Test public void update(){
		Person person=personService.getPerson(1);
		person.setName("��ĳĳ");
		personService.update(person);
	}
	@Test public void delete(){
		personService.delete(1);
	}
	@Test public void getBeans(){
		for(Person person:personService.getPerson()){
			System.out.println(person.getName());
		}
	}
}

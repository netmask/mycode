package cn.cmdoit;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import cn.cmdoit.domain.Name;
import cn.cmdoit.domain.User;

public class Base {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		User u=addUser();
		User user=getUser(u.getId());
		update();
		sql();
		//System.out.println("First name:"+user.getName().getFirstname());
	}
	static User getUser(int Id){
		Session s=null;
		try{
			s=HibernateUitl.getSession();
			Class userClass=User.class;
			//User user=(User)s.get(userClass,Id);
			User user=(User)s.load(userClass,Id);
			Hibernate.initialize(user);
			System.out.println(user.getClass());
			//System.out.println("First name:"+user.getName().getFirstname());
			//System.out.println("Last name:"+user.getName().getLastname());
			return user;
		}finally{
			if(s!=null)
				s.close();
		}
	}
	static List namedQuery(Date date){
		Session s=HibernateUitl.getSession();
		Query q=s.getNamedQuery("getUserBirthday");
		q.setDate("birthday", date);
		return q.list();
	}
	static List sql(){
		Session s=HibernateUitl.getSession();
		Query sql=s.createSQLQuery("select * from user").addEntity(User.class);
		List<User> rs=sql.list();
		for(User u:rs)
			System.out.println("--"+u.getName().getLastname());
		return rs;
	}
	static void update(){
		Session s=null;
		try{
			s=HibernateUitl.getSession();
			Transaction tx=s.beginTransaction();
			Class userClass=User.class;
			Query hql=s.createQuery("from User");
			List<User> users=hql.list();
			for(User u:users){
				u.setBirthday(new Date());
			}
			tx.commit();
		}finally{
			if(s!=null)
				s.close();
		}
	}
	static User addUser(){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			User u=new User();
			u.setBirthday(new Date());
			Name name=new Name();
			name.setFirstname("Mr.");
			name.setLastname("Liu");
			u.setName(name);
			s.save(u);
			tx.commit();
			return u;
		}finally{
			if(s!=null)
				s.close();
		}
	}
}

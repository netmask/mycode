package cn.cmdoit;

//import java.util.ArrayList;
import java.util.HashSet;
//import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.cmdoit.domain.Department;
import cn.cmdoit.domain.Employee;
import cn.cmdoit.domain.Sales;
import cn.cmdoit.domain.Skiller;

public class many2one {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			Department depart=add();
			System.out.println("end");
			queryDepart(depart.getId());
			Employee emp=query(1);
			
		}
	static Department add(){
		Session s=null;
		Transaction tx=null;
		try{
			Department depart=new Department();
			depart.setName("web");
			
			Employee emp1=new Employee();
			emp1.setDepart(depart);
			emp1.setName("xiaomin");
			
			Skiller emp2=new Skiller();
			emp2.setDepart(depart);//多个对应一个，建立关联
			emp2.setName("xiaoliu");
			emp2.setSkill("skill");
			
			Sales emp3=new Sales();
			emp3.setDepart(depart);
			emp3.setName("xiaocheng");
			emp3.setSell(100);
	
			Set<Employee> emps=new HashSet<Employee>();
			//List<Employee> emps=new ArrayList<Employee>();
			emps.add(emp1);
			emps.add(emp2);
			emps.add(emp3);
			depart.setEmps(emps);
			
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			s.save(depart);
			s.save(emp1);
			s.save(emp2);
			s.save(emp3);
			System.out.println("---------");
			tx.commit();
			return depart;
		}finally{
			if(s!=null)
				s.close();
		}

	}
	static Employee query(int empId){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			Employee emp1=(Employee)s.get(Employee.class, empId);
			System.out.println(emp1.getClass());
			Sales emp=(Sales)s.get(Sales.class, empId);
			System.out.println(""+emp.getSell()+emp.getName()+emp.getDepart().getName());
			System.out.println("depart name:"+emp.getDepart().getName());
			Hibernate.initialize(emp.getDepart());
			tx.commit();
			return emp;
		}finally{
			if(s!=null)
				s.close();
		}

	}
	
	static Department queryDepart(int departId){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			Department depart=(Department)s.get(Department.class, departId);
			System.out.println("emps size:"+depart.getEmps());
			//System.out.println("emps size:"+depart.getEmps().size());
			Hibernate.initialize(depart.getEmps());
			tx.commit();
			return depart;
		}finally{
			if(s!=null)
				s.close();
		}

	}
}

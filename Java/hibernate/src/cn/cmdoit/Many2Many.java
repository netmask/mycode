package cn.cmdoit;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.cmdoit.domain.Department;
import cn.cmdoit.domain.Employee;
import cn.cmdoit.domain.Student;
import cn.cmdoit.domain.Teacher;
import cn.cmdoit.domain.User;


public class Many2Many {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			add();
			Query(1);
			
		}
	static void add(){
		Session s=null;
		Transaction tx=null;
		try{
			Set<Teacher> ts=new HashSet<Teacher>();
			Teacher T1=new Teacher();
			T1.setName("Mr.Li");
			ts.add(T1);
			
			Teacher T2=new Teacher();
			T2.setName("Mr.Wang");
			ts.add(T2);
			
			Set<Student> ss=new HashSet<Student>();
			Student S1=new Student();
			S1.setName("xiaoli");
			ss.add(S1);
			
			Student S2=new Student();
			S2.setName("xiaowang");
			ss.add(S2);
			
			T1.setStudents(ss);
			T2.setStudents(ss);
		
			//S1.setTeachers(ts);
			//S2.setTeachers(ts);
			
			
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			s.save(T1);
			s.save(T2);
			s.save(S1);
			s.save(S2);
			tx.commit();
		}finally{
			if(s!=null)
				s.close();
		}

	}
	static void Query(int Id){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			Teacher t=(Teacher)s.get(Teacher.class, Id);
			System.out.println("student name:"+t.getStudents().size());
			//String hql="select teacher_id from Student,teacher_student  where Student.name='xiaoli' and teacher_student.student_id=student.id";
			//Hibernate.initialize(depart.getEmps());
			tx.commit();
		}finally{
			if(s!=null)
				s.close();
		}
	}

	static Employee query(int empId){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			Employee emp=(Employee)s.get(Employee.class, empId);
			//System.out.println("depart name:"+emp.getDepart().getName());
			Hibernate.initialize(emp.getDepart());
			tx.commit();
			return emp;
		}finally{
			if(s!=null)
				s.close();
		}

	}
	
	static Department queryDepart(int departId){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			Department depart=(Department)s.get(Department.class, departId);
			System.out.println("emps size:"+depart.getEmps().size());
			Hibernate.initialize(depart.getEmps());
			tx.commit();
			return depart;
		}finally{
			if(s!=null)
				s.close();
		}
	}
}

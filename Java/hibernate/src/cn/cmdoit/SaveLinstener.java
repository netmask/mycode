package cn.cmdoit;

import org.hibernate.HibernateException;
import org.hibernate.event.SaveOrUpdateEvent;
import org.hibernate.event.SaveOrUpdateEventListener;

import cn.cmdoit.domain.User;

public class SaveLinstener implements SaveOrUpdateEventListener {

	@Override
	public void onSaveOrUpdate(SaveOrUpdateEvent event)
			throws HibernateException {
		if(event.getObject() instanceof cn.cmdoit.domain.User){
			User user=(User)event.getObject();
			System.out.println("----"+user.getName().getFirstname());
		}

	}

}

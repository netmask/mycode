package cn.cmdoit;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.cmdoit.domain.IdCard;
import cn.cmdoit.domain.Person;

public class One2One {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		add();
		query(1);

	}
	static Person add(){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			IdCard idCard=new IdCard();
			idCard.setUsefulLife(new Date());
			
			Person p=new Person();
			p.setName("liuxudong");
			p.setIdCard(idCard);
			
			idCard.setPerson(p);

			s.save(p);
			s.save(idCard);
			tx.commit();
			return p;
		}finally{
			if(s!=null)
				s.close();
		}
	}
	
	static Person query(int id){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			Person p=(Person)s.get(Person.class, id);
			System.out.println("idcard usefullife:"+p.getIdCard().getUsefulLife()+"person name:"+p.getName());
			IdCard idCard=(IdCard)s.get(IdCard.class, id);
			System.out.println("person name:"+idCard.getPerson().getName());
			tx.commit();
			return p;
		}finally{
			if(s!=null)
				s.close();
		}
	}

}

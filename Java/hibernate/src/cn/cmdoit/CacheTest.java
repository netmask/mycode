package cn.cmdoit;

import java.util.Date;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.stat.Statistics;

import cn.cmdoit.domain.Name;
import cn.cmdoit.domain.User;

public class CacheTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		User u=addUser();
		getUser(u.getId());
		
		//Statistics ts=HibernateUitl.getSessionFactory().getStatistics();
		//System.out.println("Hit:"+ts.getSecondLevelCachePutCount());
		//System.out.println("Hit:"+ts.getSecondLevelCacheHitCount());
		//System.out.println("Hit:"+ts.getSecondLevelCacheMissCount());
		

	}
		static User addUser(){
			Session s=null;
			
			try{
				s=HibernateUitl.getSession();
				User u=new User();
				u.setBirthday(new Date());
				Name name=new Name();
				name.setFirstname("Mr.");
				name.setLastname("Liu");
				u.setName(name);
				s.save(u);
				return u;
			}finally{
				if(s!=null)
					s.close();
			}
		}
	static User getUser(int Id){
		Session s=null;
		User user=null;
		try{
			s=HibernateUitl.getSession();
			Class userClass=User.class;
			user=(User)s.get(userClass,Id);
			System.out.println(user.getName());
			user=(User)s.get(userClass,Id);
			System.out.println(user.getName());
			s.clear();
		}finally{
			if(s!=null)
				s.close();
		}
		try{
			s=HibernateUitl.getSession();
			Class userClass=User.class;
			user=(User)s.get(userClass,Id);
			System.out.println(user.getName());
			
		}finally{
			if(s!=null)
				s.close();
		}
		return user;
	}
}

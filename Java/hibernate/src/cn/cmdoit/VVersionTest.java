package cn.cmdoit;

import java.util.Date;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.cmdoit.domain.Name;
import cn.cmdoit.domain.User;

public class VVersionTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		User u=addUser();
		update(u.getId());

	}
	static void update(int Id){
		Session s1=null;
		Transaction tx1=null;
	
			s1=HibernateUitl.getSession();
			tx1=s1.beginTransaction();
			Class userClass=User.class;
			User user1=(User)s1.get(userClass,Id);
			user1.getName().setFirstname("version1.firstname");
			
			Session s2=HibernateUitl.getSession();
			Transaction tx2=s2.beginTransaction();
			User user2=(User)s2.get(userClass,Id);
			user2.getName().setFirstname("version2.firstname");
			
			
			tx1.commit();
			tx2.commit();
	
			s2.close();
			s1.close();
	}
	static User addUser(){
		Session s=null;
		Transaction tx=null;
		try{
			s=HibernateUitl.getSession();
			tx=s.beginTransaction();
			User u=new User();
			u.setBirthday(new Date());
			Name name=new Name();
			name.setFirstname("Mr.");
			name.setLastname("Liu");
			u.setName(name);
			s.save(u);
			tx.commit();
			return u;
		}finally{
			if(s!=null)
				s.close();
		}
	}
}



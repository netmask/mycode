package cn.cmdoit.server.impl;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import cn.cmdoit.server.PersonDao;
import cn.cmdoit.server.PersonService;

@Service("personService") @Scope("prototype")
public class PersonServiceBean implements PersonService {
    private PersonDao personDao;
    
    @PostConstruct
    public void init(){
    	System.out.println("触发Spring的初始化");
    }
    
    @PreDestroy
    public void destroy(){
    	System.out.println("销毁回调");
    }
	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}
    public void save(){
    	personDao.add();
    }
	
}

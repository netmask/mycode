package com.example.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.model.BeerExpert;

public class BeerSelect extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public BeerSelect() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*<!--
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out
				.println("Beer Selection Advice<br>");
		String c=request.getParameter("color");
		BeerExpert be=new BeerExpert();
		List result=be.getBrands(c);
		Iterator it=result.iterator();
		while(it.hasNext()){
			out.print("<br>try: "+it.next());
		}
		out.flush();
		out.close();
		--!>*/
		
		String c=request.getParameter("color");
		BeerExpert be=new BeerExpert();
		List result=be.getBrands(c);
		
		request.setAttribute("styles",result);
		RequestDispatcher view=request.getRequestDispatcher("result.jsp");
		view.forward(request, response);
		
	}

}

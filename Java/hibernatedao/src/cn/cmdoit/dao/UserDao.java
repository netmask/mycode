package cn.cmdoit.dao;
import cn.cmdoit.domain.User;
public interface UserDao {
	public void saveUser(User user);
	public User findUserByNmae(String name);
	public User findUserById(int id);
	public void updateUser(User user);
	public void remove(User user);
}

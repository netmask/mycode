package cn.cmdoit.dao;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;

public final class HibernateUitl {
	private static  SessionFactory sessionFactory;
	
	/*private static void HibernateUitl(){
		Configuration cfg=new Configuration();
		cfg.configure();
		sessionFactory=cfg.buildSessionFactory();
	}*/
	private HibernateUitl(){
		
	}
	static{
		Configuration cfg=new Configuration();
		cfg.configure();
		sessionFactory=cfg.buildSessionFactory();
	}
	public static SessionFactory getSessionFactory(){
		return sessionFactory;
	}
	public static Session getSession(){
		return sessionFactory.openSession();
	}
}

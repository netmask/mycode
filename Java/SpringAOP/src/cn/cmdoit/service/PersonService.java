package cn.cmdoit.service;

public interface PersonService {
	public void save(String name);
	public void update(String name,int id);
	public String getPersonName(String name);
}

package cn.cmdoit.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/*
@Aspect
public class MyInterceptor {
	@Pointcut("execution(* cn.cmdoit.service.impl.PersonServiceBean.*(..))")
	private void anyMethod(){}
	
	@Before("anyMethod() && args(name)")
	public void doAccessCheck(String name){
		System.out.println("前置通知"+name);
	}
	@AfterReturning(pointcut="anyMethod()",returning="result")
	public void doAfterReturning(String result){
		System.out.println("后置通知"+result);
	}
	@After("anyMethod()")
	public void doAfter(){
		System.out.println("最终通知");
	}
	@AfterThrowing(pointcut="anyMethod()",throwing="e")
	public void doAferThrowing(Exception e){
		System.out.println("例外通知"+e);
	}
	@Around("anyMethod()")
	public Object doBasicprofiling(ProceedingJoinPoint pjp) throws Throwable{
		System.out.println("进入方法");
		Object result = pjp.proceed();
		System.out.println("退出方法");
		return result;
	}
}*/

public class MyInterceptor {
	
	
	public void doAccessCheck(){
		System.out.println("前置通知");
	}
	
	public void doAfterReturning(){
		System.out.println("后置通知");
	}
	
	public void doAfter(){
		System.out.println("最终通知");
	}
	
	public void doAferThrowing(){
		System.out.println("例外通知");
	}
	
	public Object doBasicprofiling(ProceedingJoinPoint pjp) throws Throwable{
		System.out.println("进入方法");
		Object result = pjp.proceed();
		System.out.println("退出方法");
		return result;
	}
}
package cn.cmdoit.server.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;

import cn.cmdoit.server.PersonDao;
import cn.cmdoit.server.PersonService;

public class PersonServiceBean implements PersonService {
	@Resource private PersonDao personDao;
	private String name;
	public PersonServiceBean(PersonDao personDao, String name) {
		super();
		this.personDao = personDao;
		this.name = name;
	}
	/*
	private Set<String> sets=new HashSet<String>();
	private List<String> lists=new ArrayList<String>();
	private Properties properties =new Properties();
	private Map<String,String> maps=new HashMap<String,String>();
	
	public Map<String, String> getMaps() {
		return maps;
	}
	public void setMaps(Map<String, String> maps) {
		this.maps = maps;
	}
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	public List<String> getLists() {
		return lists;
	}
	public void setLists(List<String> lists) {
		this.lists = lists;
	}
	public Set<String> getSets() {
		return sets;
	}
	public void setSets(Set<String> sets) {
		this.sets = sets;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private int id;
	
	public PersonDao getPersonDao() {
		return personDao;
	}
	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}
	public void init(){
		System.out.println("初始化");
	}
	public PersonServiceBean(){
		System.out.println("我别实例化了");
	}
	*/
	public void save(){
		System.out.println("name="+name);
		System.out.println("我是save()方法");
		personDao.add();
	}
	public void destory(){
		System.out.println("关闭打开的资源");
	}
}

package com.sxt.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.sxt.po.User;
import com.sxt.service.UserService;

@Component
@RequestMapping("/user.do")
@SessionAttributes({"a","u"})
public class UserController{

	@Resource
	private UserService userService;
	
	@RequestMapping(params="method=reg")
	public String reg(String uname){
		System.out.println("UserController.reg()");
		userService.add(uname);
		System.out.println(uname);
		return "index";
	}
	
	@RequestMapping(params="method=reg2")
	public ModelAndView reg2(User user){
		System.out.println("UserController.reg2()");
		userService.add(user.getUname());
		System.out.println(user.getUname());
		ModelAndView mav=new ModelAndView("index"); 
		return mav;
	}
	
	@RequestMapping(params="method=reg3")
	public String reg3(@RequestParam("uname")String name,HttpServletRequest req, ModelMap map){
		System.out.println("UserController.reg3()");
		userService.add(name);
		System.out.println(name);
		req.getSession().setAttribute("c", "cccc");
		map.addAttribute("a", "aaaa");
		return "redirect:http://127.0.0.1/phpinfo.php";
		//return "index";
	}
	
	@RequestMapping(params="method=reg4")
	public String reg4(@ModelAttribute("a") String a,HttpServletRequest req, ModelMap map){
		System.out.println("UserController.reg4()");
		userService.add(a);
		System.out.println(a);
		return "redirect:index.jsp";
	}
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	
}